package com.me.mygdxgame;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

public class ActionResolverAndroid implements ActionResolver{
    Handler uiThread;
    Context appContext;
    
    //accesoBT mibtactivity;
    
  public ActionResolverAndroid(Context appContext) {
            uiThread = new Handler();
            this.appContext = appContext;
            
            //mibtactivity = new accesoBT(appContext);
            
    }
  
  @Override
  public void lanzamenuBT(){

	  uiThread.post(new Runnable() {
		  public void run() {
			  Toast.makeText(appContext, "este es un toast de prueba", Toast.LENGTH_LONG)
			  .show();

			  //COLOCADO COMENTARIO GLOBAL FOR NOW*****************************************************
			  /*mibtactivity.setupServiciosBT();
                  mibtactivity.checa_enciende_BT();

                  //Toast.makeText(appContext, "salio de encender BT", Toast.LENGTH_LONG).show();

                  mibtactivity.connectDevice(true, "00:15:83:07:D0:0E");
                  mibtactivity.hacer_login();*/
			  //*****************************************************************************************
		  }
	  });

	  //Intent intent = new Intent(appContext, BTMainActivity.class);
	  //appContext.startActivity(intent);

  }

  public void AbrirActividadEnRun(){
	  
	  uiThread.post(new Runnable() {
		  public void run() {
			  //Intent intent = new Intent(appContext, BTLoginActivity.class);
			  //appContext.startActivity(intent);
		  }
	  });
  }
  
  public void AbrirActividad(){
	  Intent intent = new Intent(appContext, BTLoginActivity.class);
	  appContext.startActivity(intent);
  }
  
  public void AbrirActividad_VerGrafica(){
	  Intent intent = new Intent(appContext, VerGraficaActivity.class);
	  appContext.startActivity(intent);
  }
  
  public void AbrirActividad_SyncBT(){
	  Intent intent = new Intent(appContext, SyncDataBTActivity.class);
	  appContext.startActivity(intent);
  }

}
