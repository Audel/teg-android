package com.me.mygdxgame;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class MainActivity extends AndroidApplication {
	
	//VARIABLES TRAIDES DEL EJEMPLO BT
    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_FILE_RECV = 6;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = false;
        
        //CREO EL ACTIONRESOLVER QUE SERVIRA COMO INTERFAZ CON LIBGDX
        ActionResolverAndroid actionResolver;
        //LE PASO ESTE CONTEXTO AL ACTION RESOLVER PARA QUE PUEDA FUNCIONAR
        actionResolver = new ActionResolverAndroid(this);
        
        initialize(new MyGdxGame(actionResolver), cfg);
    }
}