package com.me.mygdxgame;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

public class VerGraficaActivity extends Activity {
	//AHORA PROBANDO LAS PREFERENCIAS
	SharedPreferences prefs;
	//unas variabbles globales para despeus pasarlas a las preferencias
	String serverAddressTry, idMedicoTry, idPacienteTry, passwordMedicoTry;

	// Debugging
	private static final String TAG = "MainActivity_PruebaBluetooth";
	private static final boolean D = true;

	//VARIABLES TRAIDES DEL EJEMPLO BT
	// Message types sent from the BluetoothChatService Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;
	public static final int MESSAGE_FILE_RECV = 6;
	public static final int MESSAGE_LOGIN = 7;
	public static final int MESSAGE_PATIENTSEARCH = 8;

	// Key names received from the BluetoothChatService Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// Intent request codes
	private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
	private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
	private static final int REQUEST_ENABLE_BT = 3;

	// Name of the connected device
	private String mConnectedDeviceName = null;
	// Local Bluetooth adapter
	private BluetoothAdapter mBluetoothAdapter = null;
	//*************************************************************************************
	//ACA ESTA EL OBJETO DE LA CLASE QUE MANEJA LOS SERVICIOS BLUETOOTH
	//*************************************************************************************
	// Member object for the chat services
	private BluetoothTranService mTranService = null;
	//#########################################################################################################
	
	//variables para la gui y pedir presentacion de graficas
	private Spinner spinner1, spinner2;
	private Button btnHistorico, btnUltSesion;
	String idPaciente;
	
	//variable para la imagen donde deberia ir la grafica
	ImageView imageGraf;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ver_grafica);
		
		//******************
		//PRIMERO SE BUSCA QUE ESXISTA EL ADAPTADOR BLUETOOTH
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth no esta disponible...", Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		
		//INICIALIZA LAS PREFERENCIAS COMPRATIDAS PARA GUARDAR
	    prefs = this.getSharedPreferences("MyPrefs", Context.MODE_WORLD_WRITEABLE);
		//prefs.getString("message", "no leyo nada");
		
		idPaciente = prefs.getString("id_paciente_activo", "0");
		
		spinner1 = (Spinner) findViewById(R.id.spinner1);
		
		addListenerOnButton_Historico();
		addListenerOnButton_UltSesion();
		
		spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				/*Toast.makeText(parent.getContext(), 
						"OnItemSelectedListener : " + parent.getItemAtPosition(pos).toString(),
						Toast.LENGTH_SHORT).show();*/
				if(pos==3){
					addItemsOnSpinner2_ver2();
				}else{
					addItemsOnSpinner2_ver1();
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			//agregamos e
		});
		
		//POR ULTIMO HACE LOS BOTONES INVISIBLES HASTA QUE ESTE COMPLETAMENTE COENCTADO
		btnHistorico.setVisibility(View.INVISIBLE);
		btnUltSesion.setVisibility(View.INVISIBLE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ver_grafica, menu);
		return true;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		if(D) Log.e(TAG, "++ ON START ++");
				
		//asignacion del imageviw
		imageGraf = (ImageView) findViewById(R.id.imageView1);

		//AHORA VERIFICA QUE ESTE ENCENDIDO
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);		   
		}

		//UNA VEZ ESTE ENCENCDIDO QUIERO VER UNOS DATOS:
		String status;
		if (mBluetoothAdapter.isEnabled() && mTranService == null) {
			String mydeviceaddress = mBluetoothAdapter.getAddress();
			String mydevicename = mBluetoothAdapter.getName();
			status = mydevicename + " : " + mydeviceaddress;

			//Si acaba de prender el Bluetooth Entonces llama al setup
			setupServiciosBT();
		}
		else
		{
			status = "Bluetooth no esta habilitado.";
		}
		//ACA MUESTRA LOS DATOS DEL BLUETOTTH PERO DESPUES LO DEBO QUITAR
		Toast.makeText(this, status, Toast.LENGTH_LONG).show();
		
		//LUEGO INTENTA HACER CONEXION DIRECTO DESDE ACA
		connectDevice(true, prefs.getString("server_address", "0:0:0:0:0:0"));		

	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		if(D) Log.e(TAG, "+ ON RESUME +");
		// Performing this check in onResume() covers the case in which BT was
		// not enabled during onStart(), so we were paused to enable it...
		// onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
		if (mTranService != null) {
			// Only if the state is STATE_NONE, do we know that we haven't started already
			if (mTranService.getState() == BluetoothTranService.STATE_NONE) {
				// Start the Bluetooth chat services
				if(D) Log.e(TAG, "DICE QUE NO HAY SERVICIO BT ACTIVO**********************************");
				mTranService.start();
			}
		}

		if(D) Log.e(TAG, "FINALIZO LA LLAMADA ON RESUME");
	}
	
	public void addItemsOnSpinner2_ver1() {

		spinner2 = (Spinner) findViewById(R.id.spinner2);
		List<String> list = new ArrayList<String>();
		list.add("tiempo de juego");
		list.add("separacion de dedos");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner2.setAdapter(dataAdapter);
	}

	public void addItemsOnSpinner2_ver2() {

		spinner2 = (Spinner) findViewById(R.id.spinner2);
		List<String> list = new ArrayList<String>();
		list.add("tiempo de juego");
		list.add("porcentaje de relleno");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner2.setAdapter(dataAdapter);
	}
	
	public void addListenerOnButton_Historico() {

		spinner1 = (Spinner) findViewById(R.id.spinner1);
		spinner2 = (Spinner) findViewById(R.id.spinner2);
		btnHistorico = (Button) findViewById(R.id.buttonHistorico);

		btnHistorico.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				String mensaje = "";
				String data_req = "";
				
				Log.d("VERGRAF", "HICISTE CLICK EN EL BOTON HISTORICO");
				
				if(spinner1.getSelectedItemPosition() == 3){
					//se selecciono el jeugo pintar
					switch (spinner2.getSelectedItemPosition()){
					case 0:
						data_req= "0^3^0";
						break;
					case 1:
						data_req= "2^3^0";
						break;
					} 
				}else{
					switch (spinner2.getSelectedItemPosition()){
					case 0:
						data_req= "0^" + spinner1.getSelectedItemPosition() + "^0";
						break;
					case 1:
						data_req= "1^" + spinner1.getSelectedItemPosition() + "^0";
						break;
					default:
						data_req= "0^0^0";
						break;
					}
				}
				
				mensaje = "GETGRAF|"+idPaciente+"|"+data_req;
				
				Log.d("VERGRAF", "EL MENSAJE ARMADO ES: " +mensaje);

				/*Toast.makeText(VerGraficaActivity.this,
						"OnClickListener : " + mensaje,	Toast.LENGTH_SHORT).show();*/
				//POR ULTIMO MADNA POR BLUETOOTH EL MENSAJE CON LA PETICION
				mTranService.write(mensaje.getBytes());
			}

		});
	}

	public void addListenerOnButton_UltSesion() {

		spinner1 = (Spinner) findViewById(R.id.spinner1);
		spinner2 = (Spinner) findViewById(R.id.spinner2);
		btnUltSesion = (Button) findViewById(R.id.buttonUltSesion);

		btnUltSesion.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				String mensaje = "";
				String data_req = "";
				
				Log.d("VERGRAF", "HICISTE CLICK EN EL BOTON ULTIMA SESION");
				
				if(spinner1.getSelectedItemPosition() == 3){
					//se selecciono el jeugo pintar
					switch (spinner2.getSelectedItemPosition()){
					case 0:
						data_req= "0^3^1";
						break;
					case 1:
						data_req= "2^3^1";
						break;
					} 
				}else{
					switch (spinner2.getSelectedItemPosition()){
					case 0:
						data_req= "0^" + spinner1.getSelectedItemPosition() + "^1";
						break;
					case 1:
						data_req= "1^" + spinner1.getSelectedItemPosition() + "^1";
						break;
					default:
						data_req= "0^0^0";
						break;
					}
				}
				
				mensaje = "GETGRAF|"+idPaciente+"|"+data_req;				

				Log.d("VERGRAF", "EL MENSAJE ARMADO ES: " +mensaje);

				/*Toast.makeText(VerGraficaActivity.this,
						"OnClickListener : " + mensaje,	Toast.LENGTH_SHORT).show();*/
				//POR ULTIMO MADNA POR BLUETOOTH EL MENSAJE CON LA PETICION
				mTranService.write(mensaje.getBytes());
			}

		});
	}
	
	//*********************************************************************************************
		//FUNCIONES DE LA ACTIVIDAD BLETOOOTH
		//*********************************************************************************************

		//EN ESTE CASO SOLO INICUALIZA LA CLASE DE LOS SERVICIOS BLUETOOTH
		public void setupServiciosBT(){

			//Primero quiero checar que el bluetooth esta encendido para ver si pongo los botones
			BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if (mBluetoothAdapter == null) {
				// Device does not support Bluetooth
				Log.d(TAG, "El dispositivo no tiene bluetooth");
				Toast.makeText(this, "El dispositivo no posee adaptador Bluetooth", Toast.LENGTH_LONG).show();
				//DEBERIA CERRAR LA APLICACION Y/O ENNEGRECER LOS OTROS BOTONES
			}

			//AHORA VERIFICA QUE ESTE ENCENDIDO
			//SI esta apagado, apaga los botones
			if (!mBluetoothAdapter.isEnabled()) {
				//Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				//startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}else{
				//Si esta encendido prende el servicio Bluetooth
				// Initialize the BluetoothChatService to perform bluetooth connections
				//aunque si ya estaba prendido solo se asugura que el servicio este corriendo
				if(mTranService == null){
					mTranService = new BluetoothTranService(this, mHandler);
				}			
			}
		}

		@Override
		public void onDestroy() {
			super.onDestroy();
			// Stop the Bluetooth chat services
			if (mTranService != null) mTranService.stop();
			if(D) Log.e(TAG, "--- ON DESTROY ---");
		}

		public void Emparejarse(View view){
			//ACA DEBE LLAMAR A LA ACTIVIDAD DE CONECTARSE QUE ABRE LA OTRA VISTA Y ETC
			Intent findDeviceIntent = null;

			//LO LANZO DIRECTOEN UNA CONEXION SEGURA
			// Launch the DeviceListActivity to see devices and do scan
			findDeviceIntent = new Intent(this, DeviceListActivity.class);
			startActivityForResult(findDeviceIntent, REQUEST_CONNECT_DEVICE_SECURE);
			//return true;
			//connectDevice(true, "00:15:83:07:D0:0E");
			//connectDevice(true, "00:1B:10:00:2A:EC");
		}

		public void EnsureDiscoverable() {
			//NOTA LOS CONTENIDOS DE ESTA FUNCION LOS MOVI A ENCENDER BLUETTOTH (PUEDE SER TEMPORAL)
			if(D) Log.d(TAG, "ensure discoverable");
			if (mBluetoothAdapter == null) mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if (mBluetoothAdapter.getScanMode() !=
					BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
				Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
				discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
				startActivity(discoverableIntent);
			}
		}

		//funcion para guardar datos en sharedpreferences
		//debe ser llamada despues de verificar que el paciente seleccionado exista
		public void GuardarPrefs(){
			Editor editor = prefs.edit();
			editor.putString("server_address", serverAddressTry);
			editor.putString("id_medico_activo", idMedicoTry);
			editor.putString("id_paciente_activo", idPacienteTry);
			editor.putBoolean("logueado_medico", true);
			editor.commit();
			Log.d("BTLoginActivity", "GUARDADAS LAS PREFERENCIAS COMPARTIDAS");
		}

		//****************************************
		//ESTE ES EL HANDLER QUE HACE LA MAGIA!!!!!!!!!
		//****************************************
		// The Handler that gets information back from the BluetoothChatService
		private final Handler mHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case MESSAGE_STATE_CHANGE:
					if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
					switch (msg.arg1) {
					case BluetoothTranService.STATE_CONNECTED:
						setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
						//AHORA SI SE USA ESTE RECEPTOR
						Log.d("PROBANDOHANDLER", "El dispositivo YA SE CONECTO");
						//LUEGO INTENTA HACER LOGIN PARA QUE PUEDA HACER TODO LO DEMAS
						hacer_login();
						break;
					case BluetoothTranService.STATE_CONNECTING:
						setStatus(R.string.title_connecting);
						break;
					case BluetoothTranService.STATE_LISTEN:
					case BluetoothTranService.STATE_NONE:
						setStatus(R.string.title_not_connected);
						break;
					}
					break;
				case MESSAGE_WRITE:
					byte[] writeBuf = (byte[]) msg.obj;
					// construct a string from the buffer
					String writeMessage = new String(writeBuf);
					//QUIERO ESCRIBIR PARA VER QUE ESTOY MANDANDO
					Log.d(TAG, writeMessage);
					//NO SE USA PORQUE NO ES CHAT
					//mConversationArrayAdapter.add("Me:  " + writeMessage);
					break;
				case MESSAGE_READ:
					byte[] readBuf = (byte[]) msg.obj;
					// construct a string from the valid bytes in the buffer
					String readMessage = new String(readBuf, 0, msg.arg1);
					readMessage = null;
					//NO SE USA PORQUE NO ES CHAT
					//mConversationArrayAdapter.add(mConnectedDeviceName+":  " + readMessage);
					break;
				case MESSAGE_DEVICE_NAME:
					// save the connected device's name
					mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
					Toast.makeText(getApplicationContext(), "Connected to "
							+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();
					break;
				case MESSAGE_TOAST:
					Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
							Toast.LENGTH_SHORT).show();
					break;
				case MESSAGE_FILE_RECV:
					//Toast.makeText(getApplicationContext(), msg.obj.toString(), Toast.LENGTH_SHORT).show();
					Toast.makeText(getApplicationContext(), "Archivo Recibido", Toast.LENGTH_SHORT).show();
					DesplegarGrafica();
					break;
				case MESSAGE_PATIENTSEARCH:
					switch (msg.arg1) {
					case 1:
						//EXITO AL BUSCAR PACIENTE SE GUARDAN LOS PREFS Y SE PUEDE DEVOLVER A LA ACTIVIDAD DE LIBGDX
						GuardarPrefs();
						finish();
						break;
					case 2:
						//FALLO EN LA BUSQUEDA DE PACIENTE Y DEBE INTENTAR DE NUEVO
						Toast.makeText(getApplicationContext(), "El paciente no existe, Busque de neuvo", Toast.LENGTH_SHORT).show();
						break;
					}
					break;
				case MESSAGE_LOGIN:
					Toast.makeText(getApplicationContext(), msg.obj.toString(), Toast.LENGTH_SHORT).show();
					switch (msg.arg1) {
					case 1:
						//login exitoso ************************************************************************
						//EN ESTE CASO INICIA LA FUNCION QUE MANDARA LOS EMNSAJES CON LOS DATOS DE LAS PARTITAS
						Log.d("PROBANDOHANDLER", "SE LOGUEO Y QUEDA MADNAR LOS DATOS DE LOS JUEGOS");
						//HACER VISIBLES BOTONES
						btnHistorico.setVisibility(View.VISIBLE);
						btnUltSesion.setVisibility(View.VISIBLE);
						break;
					case 2:
						//login fallido
						break;
					}
					break;
				}
			}
		};

		private final void setStatus(CharSequence subTitle) {
			final ActionBar actionBar = getActionBar();
			actionBar.setSubtitle(subTitle);
		}

		//ESTE ES PORQUE RECIBE LOS ID DEL RECURSO CON LOS STRINGS
		private final void setStatus(int resId) {
			final ActionBar actionBar = getActionBar();
			actionBar.setSubtitle(resId);
		}

		public void LogueaPatient_()
		{             	
			String patientId = prefs.getString("id_paciente_activo", "666");
			String mensaje = "PATIENTSEARCH|"+patientId;
			Log.d(TAG, mensaje);
			mTranService.write(mensaje.getBytes());
		}
		
		public void hacer_login(){
			//PRIMERO SI NO HAY CONEXION ESTE BOTON NO DEBERIA HACER NADA
			if(mTranService.getState() != BluetoothTranService.STATE_CONNECTED){
				Log.d(TAG, "NO HAY CONEXION ACTIVA, DEBERIA CERRAR O VA A EXPLOTAR");
				return;
			}
			String cedula = prefs.getString("id_medico_activo", "2547896");
			String medpass = prefs.getString("password_medico", "c");
			String mensaje = "LOGIN|"+cedula+"|"+medpass;
			//tambien guardo el id del medico
			idMedicoTry = cedula;
			passwordMedicoTry = medpass;
			//Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
			Log.d(TAG, mensaje);
			mTranService.write(mensaje.getBytes());
		}

		//ACA ESTAN LOS QUE RECIBEN EL RESULTADO DE LA ACTIVIDAD DE MOSTRAR DISPOSITIVOS
		public void onActivityResult(int requestCode, int resultCode, Intent data) {
			if(D) Log.d(TAG, "onActivityResult " + resultCode);
			switch (requestCode) {
			case REQUEST_CONNECT_DEVICE_SECURE:
				// When DeviceListActivity returns with a device to connect
				if (resultCode == Activity.RESULT_OK) {
					connectDevice(data, true);
				}
				break;
			case REQUEST_CONNECT_DEVICE_INSECURE:
				// When DeviceListActivity returns with a device to connect
				if (resultCode == Activity.RESULT_OK) {
					connectDevice(data, false);
				}
				break;
			case REQUEST_ENABLE_BT:
				// When the request to enable Bluetooth returns
				if (resultCode == Activity.RESULT_OK) {
					// Bluetooth is now enabled, so set up a chat session
					Log.d(TAG, "ESTA EN EL ACTIVITYRESULT Y VA A HACER SETUPSERVICIOBT");
					setupServiciosBT();
				} else {
					// User did not enable Bluetooth or an error occurred
					Log.d(TAG, "BT not enabled");
					Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
					finish();
				}
			}
		}

		private void connectDevice(Intent data, boolean secure) {
			// Get the device MAC address
			String address = data.getExtras()
					.getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
			//GUARDA LA DIRECCION PAR VER SI DESPUES SE QUEDA EN LAS PREFS
			serverAddressTry = address;
			// Get the BluetoothDevice object
			BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
			// Attempt to connect to the device
			mTranService.connect(device, secure);
		}

		public void connectDevice(boolean secure, String address) {

			//GUARDA LA DIRECCION PAR VER SI DESPUES SE QUEDA EN LAS PREFS
			serverAddressTry = address;
			BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			// Get the BluetoothDevice object
			BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
			// Attempt to connect to the device
			Toast.makeText(this, device.getName(), Toast.LENGTH_SHORT).show();
			mTranService.connect(device, secure);
		}
		
		public void DesplegarGrafica(){
			Drawable d;
			try {
				TimeUnit.MILLISECONDS.sleep(900);
				//d = Drawable.createFromStream(getAssets().open("/mnt/sdcard/charttime.jpg"), null);
				//= new File(context.getFilesDir(), filename);
				File f = new File(this.getFilesDir()+"/sendopeo.jpg");
				if(f.exists()){
					d = Drawable.createFromStream(new FileInputStream(f), null);
					imageGraf.setImageDrawable(d);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

}
