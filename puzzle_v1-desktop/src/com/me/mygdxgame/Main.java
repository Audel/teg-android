package com.me.mygdxgame;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "puzzle_v1";
		cfg.useGL20 = false;
		cfg.width = 480;
		cfg.height = 320;
		
		//Un parchesito solo para que no de errores
		ActionResolverDesktop actionResolver = new ActionResolverDesktop();
		
		new LwjglApplication(new MyGdxGame(actionResolver), cfg);
	}
}
