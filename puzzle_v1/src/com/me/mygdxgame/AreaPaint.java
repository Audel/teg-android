package com.me.mygdxgame;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

public class AreaPaint implements Disposable {

	public Pixmap pixmapArea;
	public Sprite sprite;
	public Texture pixmapAreaTex;
	
	AreaPaint(Pixmap pixmap){
		
		this.pixmapArea = pixmap;
		this.pixmapAreaTex = new Texture(new PixmapTextureData(pixmap, pixmap.getFormat(), false, false));
		//this.pixmapAreaTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		this.sprite = new Sprite(pixmapAreaTex);
	}
	
	public void project(Vector2 position, float x, float y) {
		position.set(x, y);

		float centerX = sprite.getX() + sprite.getOriginX();
		float centerY = sprite.getY() + sprite.getOriginY();

		position.add(-centerX, -centerY);

		/*position.rotate(-sprite.getRotation());

		float scaleX = pixmapArea.getWidth() / sprite.getWidth();
		float scaleY = pixmapArea.getHeight() / sprite.getHeight();

		position.x *= scaleX;
		position.y *= scaleY;*/

		position.add( //
				pixmapArea.getWidth() * 0.5f, //
				-pixmapArea.getHeight() * 0.5f //
				);

		position.y *= -1f;
	}
	
	public void updatepixmap(Vector2 puntoPixMap){
		
		puntoPixMap.x -= 273;
		puntoPixMap.y = (116 + 512) - puntoPixMap.y;
		//puntoPixMap.x -= 300;
		//puntoPixMap.y = (300 + 256) - puntoPixMap.y;
		
		//Pixmap pixmap = new Pixmap( 256, 256, Format.RGBA8888 );
		
		//this.pixmapArea.setColor( 0, 1, 0, 0.75f );
		//pixmap.fillCircle( 128, 128, 128 );
		this.pixmapArea.setColor( 1, 0, 0, 1f );
		//pixmap.drawPixel(32, 32);
		this.pixmapArea.fillCircle( (int) puntoPixMap.x, (int) puntoPixMap.y, 5);
		//pixmaptex = new Texture( pixmap, Format.RGB888, false );
		this.pixmapAreaTex = new Texture( pixmapArea );
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		this.pixmapArea.dispose();
		this.pixmapAreaTex.dispose();
	}
	
}
