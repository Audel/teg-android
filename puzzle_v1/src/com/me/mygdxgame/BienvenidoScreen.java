package com.me.mygdxgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;

public class BienvenidoScreen implements Screen{
	
	MyGdxGame mijuego;
	
	Stage mistage;
	Image ala, ala_v;
	
	Skin miskin;
	
	public BienvenidoScreen(MyGdxGame unjuego){
		this.mijuego = unjuego;
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		mistage = new Stage();
		Gdx.input.setInputProcessor(mistage);
		
		miskin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
		
		//CARGA DE ETXTURAS PARA LOS IMAGEBUTON
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/menus/AllMenuIcons.pack"));
		TextureRegion IconJuegos = atlas.findRegion("btnImage_menuJuegos");
		Image imgIconJuegos = new Image(IconJuegos);
		TextureRegion IconLogin = atlas.findRegion("btnImage_menuIrLogin");
		Image imgIconLogin = new Image(IconLogin);

		//DEFINIENDO LOS IMAGEBUTONS
		Button imgButtonJugar = new Button(imgIconJuegos, miskin);
		Button imgButtonLogin = new Button(imgIconLogin, miskin);
		
		//ACCIONES DE LOS BOTONES
		imgButtonJugar.addListener(new ChangeListener() {

			@Override
			public void changed (ChangeEvent event, Actor actor) {
				//PRIMERO SETEO EL BOOLEANO APRA DENTAR QUE NO GUARDARA DATOS NI NADA POR EL ESTILO
				//LUEGO CARGA LA PANTALLA DE SELECCION DE JUEGOS
				mijuego.FuncionesCompletas = false;
				mijuego.setScreen(new GamesMenuScreen(mijuego));
			}
		});
		imgButtonLogin.addListener(new ChangeListener() {

			@Override
			public void changed (ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				//mijuego.setScreen(new UITest(mijuego));
				//mijuego.setScreen(new ConfigJuegosScreen(mijuego));
				mijuego.setScreen(new LoginScreen(mijuego));
			}
		});

		Table mitabla = new Table(miskin);
		
		mitabla.setFillParent(true);
		mitabla.add(imgButtonJugar);
		mitabla.add(imgButtonLogin);
		
		//AGREGANDO LOS ACTORES AL STAGE
		//mistage.addActor(menubg);
		mistage.addActor(mitabla);
		
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		mistage.act(delta);
		mistage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		mistage.dispose();
		miskin.dispose();
		
	}

}
