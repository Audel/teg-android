package com.me.mygdxgame;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.esotericsoftware.tablelayout.Cell;

public class ConfigJuegosScreen implements Screen{
	
	MyGdxGame mijuego;
	
	Preferences prefs;
	//VARIABLES Y ELEMENTOS DE LA UI
	Skin skin;
	Stage stage;
	SpriteBatch batch;
	
	//variables de labels de Pinza Fina
	Label labelSeccionPinza;
	Label labelUmbralPinza;
	Slider sliderUmbralPinza;
	Label labelValorUmbralPinza;
	Label labelFigArmar;
	//variables de labels de La Fabrica
	Label labelSeccionFabrica;
	Label labelTiempoJuegoFab;
	Slider sliderTiempoJuegoFab;
	Label labelValorTiempoJuegoFab;
	Label labelVelocidadPiezas;
	Slider sliderVelocidadPiezas;
	Label labelValorVelocidadPiezas;
	//variables de labels para pintar y rellenar
	Label labelSeccionPintar;
	Label labelPorcentajeRelleno;
	Slider sliderPorcentajeRelleno;
	Label labelValorPorcentajeRelleno;
	Label labelTiempoJuegoPintar;
	Slider sliderTiempoJuegoPintar;
	Label labelValorTIempoJuegoPintar;
	
	String selectedPuzzle;
	
	public ConfigJuegosScreen(MyGdxGame unjuego){
		this.mijuego = unjuego;
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// carga componentes basicos de la pantalla
		skin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		Gdx.input.setInputProcessor(stage);
		
		prefs = Gdx.app.getPreferences("MyPrefs");
		
		//define visualizacion del skin
		ImageButtonStyle style = new ImageButtonStyle(skin.get(ButtonStyle.class));
		//style.imageUp = new TextureRegionDrawable(image);
		//style.imageDown = new TextureRegionDrawable(imageFlipped);
		
		//definiendo componentes de la GUI
		Button botonGuardar = new TextButton("Guardar", skin);
		Button botonCancelar = new TextButton("Cancelar", skin/*, "toggle"*/);
		//inicializacion seccion de pinza
		labelSeccionPinza = new Label("Pinza Fina", skin);
		labelUmbralPinza = new Label("Distancia de Pinza:", skin);
		sliderUmbralPinza = new Slider(0, 10, 1, false, skin);
		sliderUmbralPinza.setValue(mijuego.juegoValorUmbralPinza);
		labelValorUmbralPinza = new Label("0", skin);
		labelFigArmar = new Label("Figura a Armar:", skin);
		final SelectBox dropdown = new SelectBox(new String[] {"Avion", "Tren", "Camion"}, skin );
		//inicializacion de labels de La Fabrica
		labelSeccionFabrica = new Label("La Fabrica", skin);
		labelTiempoJuegoFab = new Label("Tiempo de Juego:", skin);
		sliderTiempoJuegoFab = new Slider(1, 30, 1, false, skin);
		sliderTiempoJuegoFab.setValue(mijuego.juegoTiempoJuegoFab);
		labelValorTiempoJuegoFab = new Label("0", skin);
		labelVelocidadPiezas = new Label("Velocidad de las Piezas:", skin);
		sliderVelocidadPiezas = new Slider(1, 10, 1, false, skin);
		sliderVelocidadPiezas.setValue(mijuego.juegoVelocidadPiezas);
		labelValorVelocidadPiezas = new Label("0", skin);;
		//inicializacion de labels para pintar y rellenar
		labelSeccionPintar = new Label("Pintar y Rellenar", skin);
		labelPorcentajeRelleno = new Label("Porcentaje de Relleno:", skin);;
		sliderPorcentajeRelleno = new Slider(50, 90, 1, false, skin);
		sliderPorcentajeRelleno.setValue(mijuego.juegoValorPorcentajeRelleno);
		labelValorPorcentajeRelleno = new Label("0", skin);
		labelTiempoJuegoPintar = new Label("Tiempo de Juego:", skin);
		sliderTiempoJuegoPintar = new Slider(1, 30, 1, false, skin);
		sliderTiempoJuegoPintar.setValue(mijuego.juegoValorTIempoJuegoPintar);
		labelValorTIempoJuegoPintar = new Label("0", skin);
		
		Table wrapper = new Table(skin);
		wrapper.row()/*.fill().expandX()*/;
		wrapper.add(labelSeccionPinza);
		wrapper.row()/*.fill().expandX()*/;
		wrapper.add(labelUmbralPinza);
		wrapper.add(sliderUmbralPinza);
		wrapper.add(labelValorUmbralPinza);
		wrapper.row()/*.fill().expandX()*/;
		wrapper.add(labelFigArmar);
		wrapper.add(dropdown);
		wrapper.row()/*.fill().expandX()*/;
		wrapper.add(labelSeccionFabrica);
		wrapper.row()/*.fill().expandX()*/;
		wrapper.add(labelTiempoJuegoFab);
		wrapper.add(sliderTiempoJuegoFab);
		wrapper.add(labelValorTiempoJuegoFab);
		wrapper.row()/*.fill().expandX()*/;
		wrapper.add(labelVelocidadPiezas);
		wrapper.add(sliderVelocidadPiezas);
		wrapper.add(labelValorVelocidadPiezas);
		wrapper.row()/*.fill().expandX()*/;
		wrapper.add(labelSeccionPintar);
		wrapper.row()/*.fill().expandX()*/;
		wrapper.add(labelPorcentajeRelleno);
		wrapper.add(sliderPorcentajeRelleno);
		wrapper.add(labelValorPorcentajeRelleno);
		wrapper.row()/*.left()*/;
		wrapper.add(labelTiempoJuegoPintar);
		wrapper.add(sliderTiempoJuegoPintar);
		wrapper.add(labelValorTIempoJuegoPintar);
		wrapper.row()/*.expand()*/;
		wrapper.add(botonCancelar);
		wrapper.add(botonGuardar);
		wrapper.setTransform(true);
		//wrapper.setScale(2.0f);
		wrapper.scale(0.5f);
		
		wrapper.setPosition( (stage.getWidth()/2) - (wrapper.getWidth()/2), (stage.getHeight()/2) - (wrapper.getHeight()/2) );
		
		//DEFINIENDO LA VENTANA CONTENEDORA
		/*Window window = new Window("Configuracion de Juegos", skin);
		//window.setPosition(0, 0);
		window.defaults().spaceBottom(10).spaceTop(10);
		window.row().expand();
		window.add(labelSeccionPinza);
		window.row().expand();
		window.add(labelUmbralPinza).left().bottom();
		window.add(sliderUmbralPinza).left().bottom();
		window.add(labelValorUmbralPinza).left().bottom();
		window.row().expand();
		window.add(labelFigArmar).left().bottom();
		window.add(dropdown).left().bottom();
		window.row().expand();
		window.add(labelSeccionFabrica).left().bottom();
		window.row().expand();
		window.add(labelTiempoJuegoFab).left().bottom();
		window.add(sliderTiempoJuegoFab).left().bottom();
		window.add(labelValorTiempoJuegoFab).left().bottom();
		window.row().expand();
		window.add(labelVelocidadPiezas).left().bottom();
		window.add(sliderVelocidadPiezas).left().bottom();
		window.add(labelValorVelocidadPiezas).left().bottom();
		window.row().expand();
		window.add(labelSeccionPintar).left().bottom();
		window.row().expand();
		window.add(labelPorcentajeRelleno).left().bottom();
		window.add(sliderPorcentajeRelleno).left().bottom();
		window.add(labelValorPorcentajeRelleno).left().bottom();
		window.row().expand();
		window.add(labelTiempoJuegoPintar).left().bottom();
		window.add(sliderTiempoJuegoPintar).left().bottom();
		window.add(labelValorTIempoJuegoPintar).left().bottom();
		//window.add(wrapper).left().bottom();
		window.row().expand();
		window.add(botonCancelar);
		window.add(botonGuardar);
		window.pack();*/
		
		/*window.setSize(900, 400);
		window.setTransform(true);
		window.scale(0.25f);
		window.invalidate();
		window.setPosition( (stage.getWidth()/2) - (window.getWidth()/2), (stage.getHeight()/2) - (window.getHeight()/2));*/

		//hacer un resize del window
		/*List<Cell> cells = window.getCells();
		for(int ic = 0; ic < cells.size(); ic++){
			Cell cell = cells.get(cells.size()-1);
			cell.width(400);			
		}*/
		
		//agrego la ventana al escenario
		//stage.addActor(window);
		stage.addActor(wrapper);
		
		//creo los listeners en caso de necesitarlos
		dropdown.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				Gdx.app.log("UITest", "Indice seleccionado: "+ dropdown.getSelectionIndex());
				switch (dropdown.getSelectionIndex()){
				case 0:
					selectedPuzzle = "puzzle_lvl_1.txt";
					break;
				case 1:
					selectedPuzzle = "puzzle_lvl_3.txt";
					break;
				case 2:
					selectedPuzzle = "puzzle_lvl_2.txt";
					break;
				}
				
				Gdx.app.log("UITest", "Puzzle seleccionado: "+ selectedPuzzle);
			}
		});
		
		botonCancelar.addListener( new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				mijuego.setScreen(new MainMenuScreen(mijuego));
			}
		});
		
		botonGuardar.addListener( new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				mijuego.juegoValorUmbralPinza = sliderUmbralPinza.getValue();
				mijuego.juegoTiempoJuegoFab = sliderTiempoJuegoFab.getValue();
				mijuego.juegoVelocidadPiezas = sliderVelocidadPiezas.getValue();
				mijuego.juegoValorPorcentajeRelleno = sliderPorcentajeRelleno.getValue();
				mijuego.juegoValorTIempoJuegoPintar = sliderTiempoJuegoPintar.getValue();
				
				String guardarArchivo = Float.toString(sliderUmbralPinza.getValue()) + " " + 
						Float.toString(sliderTiempoJuegoFab.getValue()) + " " +
						Float.toString(sliderVelocidadPiezas.getValue()) + " " +
						Float.toString(sliderPorcentajeRelleno.getValue()) + " " +
						Float.toString(sliderTiempoJuegoPintar.getValue());
				
				FileHandle fhValores = Gdx.files.local("data/config/valores.txt");
				fhValores.writeString(guardarArchivo, false);
				
				prefs.putString("selected_puzzle", selectedPuzzle);
				prefs.flush();
				
				mijuego.setScreen(new MainMenuScreen(mijuego));
				
			}
		});
		
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		//actualizo los valors de los labels con los actuales de los sliders
		labelValorUmbralPinza.setText( Float.toString(sliderUmbralPinza.getValue()) );
		labelValorTiempoJuegoFab.setText( Float.toString(sliderTiempoJuegoFab.getValue()) );
		labelValorVelocidadPiezas.setText( Float.toString(sliderVelocidadPiezas.getValue()) );
		labelValorPorcentajeRelleno.setText( Float.toString(sliderPorcentajeRelleno.getValue()) );
		labelValorTIempoJuegoPintar.setText( Float.toString(sliderTiempoJuegoPintar.getValue()) );
		
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
		skin.dispose();
		
	}

}
