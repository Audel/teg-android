package com.me.mygdxgame;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class DebugPieza {

	public static final float SIZE = 100f;
	
	Vector2 	position = new Vector2();
	Rectangle 	bounds = new Rectangle();
	Vector3		colorrgb = new Vector3();
	Vector2		pos_target_fig = new Vector2();
	public int colorEnInt;
	
	public DebugPieza(Vector2 pos) {
		this.position = pos;
		this.bounds.width = SIZE;
		this.bounds.height = SIZE;
		this.bounds.x = pos.x;
		this.bounds.y = pos.y;
	}
	
	public void set_size(float width, float height){
		this.bounds.width = width;
		this.bounds.height = height;
	}
	
	public void setPosition(Vector2 pos){
		this.position = pos;
		this.bounds.x = pos.x;
		this.bounds.y = pos.y;
	}

	public Vector2 getPosition() {
		return position;
	}
	
	public void setColor(Vector3 uncolor){
		this.colorrgb = uncolor;
	}
	
	public void set_pos_target(Vector2 pos){
		this.pos_target_fig = pos;
	}
	
	public Vector3 getColor(){
		return this.colorrgb;
	}

	public Rectangle getBounds() {
		return bounds;
	}
	
	public boolean enPosicion(){
		if(position.dst(pos_target_fig) < 50){
			return true;
		}else return false;
	}
}
