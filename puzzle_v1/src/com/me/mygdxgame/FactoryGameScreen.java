package com.me.mygdxgame;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class FactoryGameScreen implements Screen{

	MyGdxGame mijuego;

	Stage mistage;
	ShapeRenderer mishaperenderer;
	ShapeRenderer mishaperenderer_dos;
	//Las preferencias para comunicarse con las actividades del API ANDROID
	Preferences prefs;

	PiezaFabrica piezaprueba;
	Array<PiezaFabrica> lineaPartes;
	long lastPartTime;
	Array<PiezaFabrica> puzzlearmado;
	int cont, piezaenuso;
	Array<TextureRegion> imgsPartesPosibles;
	
	DebugPieza cajaObjetivoBounds;
	
	//USARE DOS CONTADORES
	int contFigTodas;
	int contFigAcertadas;

	SpriteBatch spriteBatch;
	SpriteBatch batchPiezas;
	//Texture spriteTexture;
	Sprite sprite;
	float scrollTimer = 0.0f;
	int imgContador;
	
	//TEMPORIZADORES
	float elapsedTime;
	long actionBeginTime;
	
	//***************************ESTADISTICAS DE JUEGO*****************************************
	FileHandle file_est;
	String chorro_estadisticas;
	long startTime;
	long distPromedioDedos;
	long tiempoCompletacion;
	int numMovimientos;
	int cierresCompletoPinza;
	private FPSLogger fpsLogger;

	FactoryGameScreen(MyGdxGame unjuego){

		this.mijuego = unjuego;
		//*******************CAMBIAR DESPUES POR TEMPORIZADOR********************
		//this.arch_level = new String("puzzlevistela.txt");
	}

	public Vector3 puntomedio(Vector3 vA, Vector3 vB){
		Vector3 retorno = new Vector3();
		retorno.set((vA.x+vB.x)/2, (vA.y+vB.y)/2, (vA.z+vB.z)/2);	
		return(retorno);
	}
	
	public class PiezaFabrica extends DebugPieza{

		public PiezaFabrica(Vector2 pos) {
			super(pos);
		}
		
		public int indice_figura;
	}

	private void spawnPart() {

		//PiezaFabrica parte = new PiezaFabrica(new Vector2(0,mistage.getHeight()/2));
		PiezaFabrica parte = new PiezaFabrica(new Vector2(0f,335f));
		parte.indice_figura = MathUtils.random(0, imgsPartesPosibles.size-1);
		//parte.set_size(100, 100);
		lineaPartes.add(parte);
		
		lastPartTime = TimeUtils.nanoTime();

	}


	@Override
	public void show() {
		// Cargando las preferencias
	    prefs = Gdx.app.getPreferences("MyPrefs");

		mistage = new Stage();
		Gdx.input.setInputProcessor(mistage);
		mishaperenderer = new ShapeRenderer();
		mishaperenderer_dos =  new ShapeRenderer();
		
		fpsLogger = new FPSLogger();

		//CARGA DE IMAGENES
		//TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/" + nombre_pack));
		//TextureRegion bg_1 = atlas.findRegion(lineas_file[1]);
		//Image bg_puzzle = new Image(bg_1);
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/juego_fabrica/fabrica.pack"));
		TextureRegion bg_1 = atlas.findRegion("fondo_fabrica");
		Image bg_fabrica = new Image(bg_1);
		TextureRegion cinta_completa = atlas.findRegion("cinta_rodillos");
		Image img_cinta = new Image(cinta_completa);
		img_cinta.setPosition(0, 236);
		TextureRegion caja_obj = atlas.findRegion("obj_cajon");
		Image caja_objetivo = new Image(caja_obj);
		caja_objetivo.setPosition((mistage.getWidth()/2) - caja_objetivo.getWidth()/2, 0);
		mistage.addActor(bg_fabrica);
		mistage.addActor(img_cinta);
		mistage.addActor(caja_objetivo);
		//********
		cajaObjetivoBounds = new DebugPieza(new Vector2((mistage.getWidth()/2) - caja_objetivo.getWidth()/2, 0));
		cajaObjetivoBounds.set_size(300, 300);
		//************* se necesita un arreglo con todas las imagenes posibles o disponibles que pueden aparecer*******************
		imgsPartesPosibles = new Array<TextureRegion>();
		imgsPartesPosibles.add( new TextureRegion( atlas.findRegion("caja")) );
		imgsPartesPosibles.add( new TextureRegion( atlas.findRegion("engranaje")) );
		imgsPartesPosibles.add( new TextureRegion( atlas.findRegion("rivet")) );
		imgsPartesPosibles.add( new TextureRegion( atlas.findRegion("tuerca")) );
		imgsPartesPosibles.add( new TextureRegion( atlas.findRegion("screwbolt")) );

		Texture tex_cinta_mov = cinta_completa.getTexture();
		tex_cinta_mov.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		sprite = new Sprite(tex_cinta_mov, 0, 0, 1280, 96);
		//sprite.setSize(1280, 180);
		sprite.setPosition(0, 317);
		spriteBatch = new SpriteBatch();
		batchPiezas = new SpriteBatch();
		
		//inicializando los contadorcillos
		contFigTodas = 0;
		contFigTodas = 0;

		//inicializa la linea de produccion de partes
		lineaPartes = new Array<PiezaFabrica>();
		spawnPart();
		
		//COMIENZA EL CONTADOR
		actionBeginTime = System.nanoTime();
		//***********************************INICIA VARIABLES PARA ESTADISTICAS*******************************************
		file_est = Gdx.files.external("DatosGames.txt");
		chorro_estadisticas = new String();
		startTime = TimeUtils.nanoTime();
		distPromedioDedos = -1;
		tiempoCompletacion = (long) mijuego.juegoTiempoJuegoFab;
		numMovimientos = 0;
		cierresCompletoPinza = 0;
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		mistage.act();
		mistage.draw();

		//HACIENDO EL SPRITE MOVERESE
		scrollTimer+=Gdx.graphics.getDeltaTime();
		if(scrollTimer>0.1f)
			scrollTimer = 0.0f;

		sprite.setU(scrollTimer+0.5f);
		sprite.setU2(scrollTimer);

		spriteBatch.begin();
		sprite.draw(spriteBatch);
		spriteBatch.end();

		//mishaperenderer.begin(ShapeType.FilledRectangle);
		mishaperenderer.begin(ShapeType.Rectangle);
		if(TimeUtils.nanoTime() - lastPartTime > 1000000000){
			spawnPart();
		}
		Iterator<PiezaFabrica> iter = lineaPartes.iterator();
		
		while(iter.hasNext()) {
			PiezaFabrica movPart = iter.next();
			//si hay input del usuario hay que verificar si se esta tocando la pieza
			movPart.setColor(new Vector3(0.0f, 1.0f, 0.0f));
			//CONTROL DE IMPUTS DEL SUUARIO <-------------------------------------------------------------
			//AHORA DETECTA DOS INPUTS
			if(Gdx.input.isTouched() &&  Gdx.input.isTouched(1)){
				Vector3 punto = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0.0f);
				Vector3 puntoDos = new Vector3(Gdx.input.getX(1), Gdx.input.getY(1), 0.0f);
				if(punto.dst(puntoDos) < 140){
					//guarda el valor de la distancia entre losd os dedos
					numMovimientos++;
					if (distPromedioDedos == 0)
						distPromedioDedos = (long) punto.dst(puntoDos);
					distPromedioDedos = (long) ((distPromedioDedos + punto.dst(puntoDos)) / 2);
					//LA PINZA TIENE DISTANCIA MENOR AL UMBRAL
					mistage.getCamera().unproject(punto);
					mistage.getCamera().unproject(puntoDos);
					//calcula punto medio para el siguiente paso
					punto = puntomedio(punto, puntoDos);
					//AHORA pREGUNTA SI EL PUNTO MEDIO DE LA PINZA ESTA EN LA PIEZA ACTUAL
					if(movPart.bounds.contains(punto.x,punto.y)){
						movPart.setColor(new Vector3(1.0f, 0.0f, 0.0f));
						movPart.setPosition(new Vector2(punto.x-50,punto.y-50));
						//ACA SE PREGUNTA SI SE COLOCA LA PARTE EN LA CAJA
						if(cajaObjetivoBounds.bounds.contains(movPart.bounds)){
							//de hecho no importa uqe lo oculte porque cuando su posicion sea mayor que screen width se saca del arreglo
							movPart.setPosition(new Vector2(-200f,-100f));
							//FIXME no esta sacando la pieza de� arreglo, solo la esta poniendo en un area no visible
							contFigTodas++;
							contFigAcertadas++;
						}
					}
				}
				
			}

			//si la pieza no esta en uso se puede seguir moviendo dentro de la linea
			if((movPart.getPosition().y > 322) && (movPart.getPosition().y + 50 < 418) ){
			//Si esta en la cinta transportadora se
				movPart.setPosition(new Vector2(5 + movPart.getPosition().x, movPart.getPosition().y));
				//movPart.setPosition(new Vector2(200 * Gdx.graphics.getDeltaTime(), movPart.getPosition().y));
				if(movPart.getPosition().x > mistage.getWidth()) iter.remove();
				//aca hago el renderizado aprovechando elciclo de actualizar posicion
				mishaperenderer.setColor(movPart.getColor().x,movPart.getColor().y,movPart.getColor().z,1.0f);
				//mishaperenderer.filledRect(movPart.getPosition().x, movPart.getPosition().y, 100, 100);
				mishaperenderer.rect(movPart.getPosition().x, movPart.getPosition().y, 100, 100);
			}else{
				// la pieza se encuentra fuera de la cinta asi que caer�
				movPart.setPosition(new Vector2(movPart.getPosition().x, movPart.getPosition().y - 5 ));
				if(movPart.getPosition().y < -200){
					//si cae demasiado bajo se remueve
					iter.remove();
				}
			}
			
			//DESPLIEGUE Y MOVIMIENTO DE LAS IMAGENES DE PRUEBA
			batchPiezas.begin();
				batchPiezas.draw(imgsPartesPosibles.get(movPart.indice_figura), movPart.getPosition().x, movPart.getPosition().y);
			batchPiezas.end();
			
		}
		mishaperenderer.end();
		
		//CONTADORES 
		elapsedTime=(System.nanoTime()-actionBeginTime)/1000000000.0f;
        if(elapsedTime>mijuego.juegoTiempoJuegoFab*15){
        	Gdx.app.log("MyTag", "el timepo de juego es "+ elapsedTime);
        	Gdx.app.log("MyTag", "las piezas en el arreglo son "+ lineaPartes.size);
        	actionBeginTime = System.nanoTime();
        	//CONDICION DE PARADA DEL JUEGO
        	//acomodando datos para guardar en archivo
			chorro_estadisticas = "tiempo="+ String.valueOf(elapsedTime/60)+"^movimientos="+String.valueOf(numMovimientos)+
					"^promedios="+String.valueOf(distPromedioDedos)+"^aciertos="+String.valueOf(contFigAcertadas)+"^puntaje=0";
			String idG = "1003";
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			String msgHeader = "ADDGAMEDATA|"+prefs.getString("id_paciente_activo")+"|"+ timeStamp +"|"+idG+"|"+chorro_estadisticas;
			Gdx.app.log("JUEGO", "guardando en archivo: "+msgHeader );
			file_est.writeString(msgHeader+"\n", true);
			mijuego.setScreen(new VictoryScreen(mijuego));
        }

        //POR ULTIMO HACEMOS LOG DE LOS FPS 
        fpsLogger.log();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}


	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub0
		mistage.dispose();
	}

}
