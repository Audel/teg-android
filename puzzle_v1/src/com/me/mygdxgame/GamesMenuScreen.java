package com.me.mygdxgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;

public class GamesMenuScreen implements Screen{
	
	MyGdxGame mijuego;
	Preferences prefs;
	
	Stage mistage;
	Image ala, ala_v;
	
	Skin miskin;
	
	//Button startgame;
	TextButton playPuzzleBtn;
	TextButton playVistelaBtn;
	TextButton playFabricaBtn;
	TextButton playPintarBtn;
	TextButton exitButton;
	
	Boolean poderJogarPuzzle;
	
	public GamesMenuScreen(MyGdxGame unjuego)
	{
		this.mijuego = unjuego;
	}

	@Override
	public void show() {
		mistage = new Stage();
		Gdx.input.setInputProcessor(mistage);
		
		prefs = Gdx.app.getPreferences("MyPrefs");
		
		miskin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
		
		poderJogarPuzzle = false;
		
		Table mitabla = new Table(miskin);
		
		playPuzzleBtn = new TextButton("Jugar Puzzle", miskin);
		playVistelaBtn = new TextButton("Jugar Vistela", miskin, "toggle");
		playFabricaBtn = new TextButton("Jugar Fabrica", miskin);
		playPintarBtn = new TextButton("Jugar Pintar", miskin, "toggle");
		exitButton = new TextButton("Salir", miskin, "toggle");
		
		playPuzzleBtn.addListener(new ChangeListener() {
			
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				mijuego.auxLvl = prefs.getString("selected_puzzle", "puzzle_lvl_1.txt");
				//mijuego.setScreen(new PuzzleGameScreen(mijuego));
				poderJogarPuzzle = true;
			}
		});
		
		playVistelaBtn.addListener(new ChangeListener() {
			
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				mijuego.auxLvl = new String("puzzlevistela.txt");
				//mijuego.setScreen(new PuzzleGameScreen(mijuego));
				poderJogarPuzzle = true;
			}
		});
		
		playFabricaBtn.addListener(new ChangeListener() {

			@Override
			public void changed (ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				mijuego.setScreen(new FactoryGameScreen(mijuego));
			}
		});

		playPintarBtn.addListener(new ChangeListener() {

			@Override
			public void changed (ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				mijuego.setScreen(new PintarGameScreen(mijuego));
			}
		});
		
		exitButton.addListener(new ChangeListener() {
			
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				Gdx.app.exit();
			}
		});
		
		Image menubg = new Image(new Texture(Gdx.files.internal("data/imgs/menu_bg.jpg")));
		menubg.setSize(1280, 720);
		
		mitabla.setFillParent(true);
		mitabla.add(playPuzzleBtn).width(150).height(50);
		mitabla.row();
		mitabla.add(playVistelaBtn).width(150).height(50).padTop(10);
		mitabla.row();
		mitabla.add(playFabricaBtn).width(150).height(50).padTop(10);
		mitabla.row();
		mitabla.add(playPintarBtn).width(150).height(50).padTop(10);
		mitabla.row();
		mitabla.add(exitButton).width(150).height(50).padTop(10);
		
		mistage.addActor(menubg);
		mistage.addActor(mitabla);
		
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		if(poderJogarPuzzle){
			mijuego.setScreen(new PuzzleGameScreen(mijuego));
		}

		mistage.act(delta);
		mistage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		mistage.dispose();
		miskin.dispose();
		
	}

}
