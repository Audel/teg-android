package com.me.mygdxgame;

import java.io.IOException;
import java.net.InetAddress;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.security.*;

public class LoginScreen implements Screen{
	
	//CLASES DE NETWORKING
	//Client object
	Client client;
	//Ports to listen on
	static int udpPort = 27967, tcpPort = 27967;
	
	MyGdxGame mijuego;
	
	Boolean pasarAMenu, verPatientSearch;
	//Las preferencias para comunicarse con las actividades del API ANDROID
	Preferences prefs;
	String serverAddressTry, idMedicoTry, idPacienteTry, passwordMedicoTry;
	
	//VARIABLES Y ELEMENTOS DE LA UI
	Skin skin;
	Stage stage;
	SpriteBatch batch;
	
	//TUVE QUE DECLARARLAS GLOABLES A VER
	Window window, windowPS;
	Table mitabla;
	TextField textfield, textfieldPatientId;
	TextField passwordTextField;
	
	public LoginScreen(MyGdxGame unjuego){
		this.mijuego = unjuego;
	}
	
	public static class PacketMessage{
		String message;
	}
	
	public String createStringHash(String passwordToHash){
        //String passwordToHash = "password";
        String generatedPassword = null;
        try {
                // Create MessageDigest instance for MD5
                MessageDigest md = MessageDigest.getInstance("MD5");
                //Add password bytes to digest
                md.update(passwordToHash.getBytes());
                //Get the hash's bytes 
                byte[] bytes = md.digest();
                //This bytes[] has bytes in decimal format;
                //Convert it to hexadecimal format
                StringBuilder sb = new StringBuilder();
                for(int i=0; i< bytes.length ;i++)
                {
                        sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
                }
                //Get complete hashed password in hex format
                generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) 
        {
                e.printStackTrace();
        }
        //System.out.println(generatedPassword);
        return(generatedPassword);
    }
	
	public void GuardarPrefs(){
    	prefs.putString("server_address", serverAddressTry);
		prefs.putString("id_medico_activo", idMedicoTry);
		prefs.putString("id_paciente_activo", idPacienteTry);
		prefs.putBoolean("logueado_medico", true);
    	prefs.flush();
    	Gdx.app.log("LoginScreen", "GUARDADAS LAS PREFERENCIAS COMPARTIDAS");
    }

	@Override
	public void show() {
		// TODO Auto-generated method stub
		//******************************************************
		//INICIALIZACION DEL CLIENTE EN LA RED
		//******************************************************
		client = new Client();
	    client.start();
	    
	    pasarAMenu = false;
	    verPatientSearch = false;
	    prefs = Gdx.app.getPreferences("MyPrefs");
	    
	    //registro de la clase
	    client.getKryo().register(PacketMessage.class);
	    //e inicializa sus variables
	    serverAddressTry = new String();
	    idMedicoTry = new String(); 
	    idPacienteTry = new String();
	    passwordMedicoTry = new String();
	    
	    //CAPTURA DEL INPUT DEL BOTON BACK
	    Gdx.input.setCatchBackKey(true);
		
		// carga componentes basicos de la pantalla
		skin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		Gdx.input.setInputProcessor(stage);		
		//define visualizacion del skin
		ImageButtonStyle style = new ImageButtonStyle(skin.get(ButtonStyle.class));

		//CARGA DE ETXTURAS PARA LOS IMAGEBUTON
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/menus/AllMenuIcons.pack"));
		TextureRegion IconBluetooth = atlas.findRegion("btnImage_ConectBluetooth");
		Image imgIconBluetooth = new Image(IconBluetooth);
		TextureRegion IconWiFi = atlas.findRegion("btnImage_ConectWiFi");
		Image imgIconWiFi = new Image(IconWiFi);
		//DEFINIENDO LOS IMAGEBUTONS
		Button imgButtonBluetooth = new Button(imgIconBluetooth, skin);
		Button imgButtonWiFi = new Button(imgIconWiFi, skin);
		
		//definiendo componentes de la GUI
		Button botonAceptar = new TextButton("Ingresar", skin);
		Button botonCancelar = new TextButton("Cancelar", skin/*, "toggle"*/);
		//un boton adicional para buscar paciente
		Button botonBuscaPatient = new TextButton("Buscar Paciente", skin);
		
		//componentes de texto
		final Label usuarioLabel = new Label("Usuario: ", skin);
		textfield = new TextField("", skin);
		final Label passwordLabel = new Label("Contrase�a: ", skin);
		passwordTextField = new TextField("", skin);
		//passwordTextField.setMessageText("password");
		passwordTextField.setPasswordCharacter('*');
		passwordTextField.setPasswordMode(true);
		//label y texfield de busqueda de paciente
		final Label patientSearchLabel = new Label("Ingrese ID de Paciente: ", skin);
		textfieldPatientId = new TextField("", skin);
		
		//TABLA CON LOS BOTONES DE LA SELECCION DE MEDIO DE COMUNICACI�N
		mitabla = new Table(skin);
		
		mitabla.setFillParent(true);
		mitabla.add(imgButtonBluetooth);
		mitabla.add(imgButtonWiFi);
		
		//DEFINIENDO LA VENTANA CONTENEDORA
		window = new Window("Login Personal Autorizado", skin);
		window.defaults().spaceBottom(10);
		window.row();
		window.add(usuarioLabel);
		window.add(textfield);
		window.row();
		window.add(passwordLabel);
		window.add(passwordTextField);
		window.row();
		window.add(botonCancelar).right();
		window.add(botonAceptar);
		window.pack();
		
		window.setPosition( (stage.getWidth()/2) - (window.getWidth()/2), 
					((stage.getHeight()/2) - (window.getHeight()/2)) + 200);
		
		//Seteando el tama�o y posicion de la vemtana
		//window.setWidth(420);
		window.setPosition( (stage.getWidth()/2) - (window.getWidth()/2), (stage.getHeight()/2) - (window.getHeight()/2));
		
		//la ventana de busqueda de paciente
		windowPS = new Window("Debe Habilitar un Paciente", skin);
		windowPS.defaults().spaceBottom(10);
		windowPS.row();
		windowPS.add(patientSearchLabel);
		windowPS.row();
		windowPS.add(textfieldPatientId);
		windowPS.row();
		windowPS.add(botonBuscaPatient);
		windowPS.pack();
		windowPS.setPosition( (stage.getWidth()/2) - (windowPS.getWidth()/2), 
				((stage.getHeight()/2) - (windowPS.getHeight()/2)) + 200);
		
		//DEBE HACER UNA PRUEBA CON EL ULTIMO MEDIO DE CONECCION PARA DETERMINAR SI PUEDE VOLVER A USARSE		
		if(Gdx.files.local("data/config/wireless.txt").exists()){
			Gdx.app.log("LOGIN_CONNECT", "el archivo existe y esta en " + Gdx.files.getLocalStoragePath());
			FileHandle fhValores = Gdx.files.local("data/config/wireless.txt");
			String valoresenFile = fhValores.readString();
			Gdx.app.log("LOGIN_CONNECT", valoresenFile);
			String [] listaValores = valoresenFile.split(" ");
			//ahora a determinar si la conectividad es por bluetooth o wifi
			if(listaValores[0].equalsIgnoreCase("wifi")){
				//PRUEBA CONECTARSE POR WIFI
				Gdx.app.log("NETWORK_IN","intentando establecer conexion...");
				try {
					InetAddress address = client.discoverHost(tcpPort, 5000);
					System.out.println(address);
					if(address != null){
						client.connect(5000, address, tcpPort, udpPort);
						mijuego.networkType = listaValores[0];
						mijuego.networkAddress = listaValores[1];
					}else{
						Gdx.app.log("NETWORK_IN","NO SE ENCONTRO EL PUERTO DEL HOST, INTENTANDO DIRECTAMENTE");
						//EL SEGUNDOO VALOR DEL ARCHIVO ES LA DIRECCION Y POR ALLI DEBERIA CONECTARSE
						client.connect(5000, listaValores[1], tcpPort, udpPort);
					}
				} catch (IOException e) {
					// EN ESTE CASO FALLO LA CONEXION POR ENDE DEBE MOSTRAR CUAL DE LOS METODOS USAR
					e.printStackTrace();
					Gdx.app.log("ERROR_NETWORK", e.toString());
					//MOSTART MENUS
					window.setVisible(false);
					windowPS.setVisible(false);
					mitabla.setVisible(true);
				}
			}
		}else{
			//NO HAY ARCHIVO CREADO SE DEBE HACER UNO NUEVO Y SE DEBE MOSTRAR LA PANTALLA DE SELECCION
			Gdx.app.log("LOGIN_CONNECT", "el archivo NO EXISTE EN " + Gdx.files.getLocalStoragePath());
			window.setVisible(false);
			windowPS.setVisible(false);
			mitabla.setVisible(true);
		}
		
		
		//agrego la ventana al escenario
		stage.addActor(windowPS);
		stage.addActor(window);
		stage.addActor(mitabla);
		
		imgButtonBluetooth.addListener( new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				// HIZO CLICK EN EL BOTON DE BLUETOOTH
				
				//probando lanzar actividad bluetooth desde aca
				mijuego.actionResolver.AbrirActividad();
				//FIXME solo una prueba, luego quitar
				//mijuego.actionResolver.AbrirActividad_VerGrafica();
				
				//DE MOMENTO NO HAY BLUETOOTH
				/*new Dialog("En Desarrollo", skin, "dialog") {
					protected void result (Object object) {
						System.out.println("Chosen: " + object); 
					}
				}.text("De momento no es posible conectarse por este medio").button("Ok", true).key(Keys.ENTER, true)
				.key(Keys.ESCAPE, false).show(stage);*/
				
			}
		});
		
		imgButtonWiFi.addListener( new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				// HIZO CLICK EN EL BOTON DE WIFI
				try {
					InetAddress address = client.discoverHost(tcpPort, 5000);
					System.out.println(address);
					if(address != null){
						client.connect(5000, address, tcpPort, udpPort);
						mijuego.networkType = "wifi";
						mijuego.networkAddress = address.toString();
					}else{
						Gdx.app.log("NETWORK_IN","NO SE ENCONTRO EL PUERTO DEL HOST, INTENTANDO DIRECTAMENTE");
						//E SERVIDOR NO ESTA DISPONIBLE Y DEBE MOSTRAR UN MENSAJE DE ERROR
						new Dialog("Error de Conectividad", skin, "dialog") {
							protected void result (Object object) {
								System.out.println("Chosen: " + object); 
							}
						}.text("No es posible conectarse por este medio").button("Ok", true).key(Keys.ENTER, true)
						.key(Keys.ESCAPE, false).show(stage);
						
					}
				} catch (IOException e) {
					// EN ESTE CASO FALLO LA CONEXION POR ENDE DEBE MOSTRAR CUAL DE LOS METODOS USAR
					e.printStackTrace();
					Gdx.app.log("ERROR_NETWORK", e.toString());
				}
			}
		});
		
		botonCancelar.addListener( new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				mijuego.setScreen(new BienvenidoScreen(mijuego));
			}
		});
		
		botonAceptar.addListener( new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				// EN ESTE CASO DEBE MANDAR LA INFORMACION CON LOS DATOS DE LOS TEXFIELDS
				Gdx.app.log("CONNECT_LOG", "LOGIN|"+textfield.getText() + "|" + createStringHash(passwordTextField.getText()) );
				//ACA GUARDAMMOS LOS TRY PARA DATOS DEL MEDICO
				idMedicoTry = textfield.getText();
				passwordMedicoTry = createStringHash(passwordTextField.getText());
				PacketMessage sendlogin = new PacketMessage();
				sendlogin.message = "LOGIN|"+textfield.getText() + "|" + createStringHash(passwordTextField.getText());
				client.sendTCP(sendlogin);
				
			}
		});
		
		botonBuscaPatient.addListener( new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				// EN ESTE CASO DEBE MANDAR LA INFORMACION CON LOS DATOS DE LOS TEXFIELDS
				Gdx.app.log("CONNECT_LOG", "PATIENTSEARCH|"+textfieldPatientId.getText() );
				//ACA GUARDAMMOS LOS TRY PARA DATOS DEL MEDICO
				idPacienteTry = textfieldPatientId.getText();
				//Gdx.app.log("TEST", "se guardo en idpatienttry " + idPacienteTry +" que deberia tener "+ textfieldPatientId.getText());
				PacketMessage sendPatientSearch = new PacketMessage();
				sendPatientSearch.message = "PATIENTSEARCH|"+textfieldPatientId.getText();
				client.sendTCP(sendPatientSearch);
			}
		});
		
		textfield.setTextFieldListener(new TextFieldListener() {
			
			public void keyTyped (TextField textField, char key) {
				if (key == '\n') textField.getOnscreenKeyboard().show(false);
			}
		});
		
		textfieldPatientId.setTextFieldListener(new TextFieldListener() {

			public void keyTyped (TextField textField, char key) {
				if (key == '\n') textField.getOnscreenKeyboard().show(false);
			}
		});
		
		passwordTextField.setTextFieldListener(new TextFieldListener() {
			public void keyTyped (TextField textField, char key) {
				if (key == '\n') textField.getOnscreenKeyboard().show(false);
				window.setPosition( (stage.getWidth()/2) - (window.getWidth()/2), (stage.getHeight()/2) - (window.getHeight()/2));
			}
		});
	    
		client.addListener(new Listener() {
			public void received (Connection connection, Object object) {
				if (object instanceof PacketMessage) {
					PacketMessage response = (PacketMessage)object;
					System.out.println(response.message);
					Gdx.app.log("NETWORK_IN", "recibio: " + response.message);
					//EN ESTE CASO RECIBIO BIEN TODO ASI QUE PUEDE PRESENTAR LA VENTANA DE LOGIN
					mitabla.setVisible(false);
					if(!verPatientSearch){
						window.setVisible(true);
					}else{
						windowPS.setVisible(true);
					}					
					if(response.message.contains("|")){
						Gdx.app.log("NETWORK_IN", "ES UN MENSAJE CON RESPUESTA DE LOGIN!!!!!!!!!!! =D");
						String[] msgPicado = response.message.split("\\|");
						//Primero chequear si el mensaje es respuesta al LOGIN
						if((msgPicado[1].equalsIgnoreCase("ok")) && (msgPicado[0].equalsIgnoreCase("loginr"))){
							mijuego.FuncionesCompletas = true;
							//mijuego.setScreen(new MainMenuScreen(mijuego));
							verPatientSearch = true;
							window.setVisible(false);
							windowPS.setVisible(true);
						}else if((msgPicado[1].equalsIgnoreCase("fail")) && (msgPicado[0].equalsIgnoreCase("loginr"))){
							new Dialog("Login Incorrecto", skin, "dialog") {
								protected void result (Object object) {
									System.out.println("Chosen: " + object); 
								}
							}.text("Usuario o Password Invalidos").button("Ok", true).key(Keys.ENTER, true)
							.key(Keys.ESCAPE, false).show(stage);
						}
						//SI NO FUE RESPEUSTA DEL LOGIN SE VERIFICA QUE SEA RESPUESTA DE PATIENTSEARCH
						if((msgPicado[1].equalsIgnoreCase("ok")) && (msgPicado[0].equalsIgnoreCase("patientsearchr"))){
							//YA ESTA LOGUEADO UN MEDICO Y ENCONTRO AL APCIENTE, ASI QUE PUEDE IR AL MENU
							pasarAMenu = true;
							GuardarPrefs();
							client.close();
						}else if((msgPicado[1].equalsIgnoreCase("fail")) && (msgPicado[0].equalsIgnoreCase("patientsearchr"))){
							new Dialog("El paciente no existe", skin, "dialog") {
								protected void result (Object object) {
									System.out.println("Chosen: " + object); 
								}
							}.text("Id de paciente invalido Invalidos").button("Ok", true).key(Keys.ENTER, true)
							.key(Keys.ESCAPE, false).show(stage);
						}
						//FIN DE LOS IFS PARA DETERMINAR CUAL MENSAJE RECIBIO
					}
				}
			}
		});
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
		
		if(pasarAMenu || prefs.getBoolean("logueado_medico", false)){
			mijuego.FuncionesCompletas = true;
			mijuego.setScreen(new MainMenuScreen(mijuego));
		}

		if (Gdx.input.isKeyPressed(Keys.BACK)){
			// Do something
			Gdx.app.log("CAPTURE", "APRETDO EL BOTON BACK");
			mijuego.setScreen(new BienvenidoScreen(mijuego));
		}
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
		skin.dispose();
		
	}

}
