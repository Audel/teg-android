package com.me.mygdxgame;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

import sun.awt.image.PixelConverter.Bgrx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.me.mygdxgame.LoginScreen.PacketMessage;

public class MainMenuScreen implements Screen{
	
	MyGdxGame mijuego;
	//Las preferencias para comunicarse con las actividades del API ANDROID
	Preferences prefs;
	
	//COSAS DE NETWORKING
	//Client object
	Client client;
	//Ports to listen on
	static int udpPort = 27967, tcpPort = 27967;
	
	Boolean verGraficaGdx;
	
	Skin skin;
	Stage mistage;
	
	Skin miskin;
	
	//Button startgame;
	TextButton startGameButton;
	TextButton optionsButton;
	TextButton exitButton;
	
	public void SyncPorWifi(){
		//usa la conexion de kryonet para enviar los datos
		//PRUEBA CONECTARSE POR WIFI
		Gdx.app.log("NETWORK_IN","intentando establecer conexion...");
		try {
			InetAddress address = client.discoverHost(tcpPort, 5000);
			System.out.println(address);
			if(address != null){
				client.connect(5000, address, tcpPort, udpPort);
			}else{
				Gdx.app.log("NETWORK_IN","NO SE ENCONTRO EL PUERTO DEL HOST, INTENTANDO DIRECTAMENTE");
				//EL SEGUNDOO VALOR DEL ARCHIVO ES LA DIRECCION Y POR ALLI DEBERIA CONECTARSE
				client.connect(5000, prefs.getString("server_address"), tcpPort, udpPort);
			}
		} catch (IOException e) {
			// EN ESTE CASO FALLO LA CONEXION POR ENDE DEBE MOSTRAR CUAL DE LOS METODOS USAR
			e.printStackTrace();
			Gdx.app.log("ERROR_NETWORK", e.toString());
			//E SERVIDOR NO ESTA DISPONIBLE Y DEBE MOSTRAR UN MENSAJE DE ERROR
			new Dialog("Error de Conectividad", skin, "dialog") {
				protected void result (Object object) {
					System.out.println("Chosen: " + object); 
				}
			}.text("No es posible conectarse por este medio").button("Ok", true).key(Keys.ENTER, true)
			.key(Keys.ESCAPE, false).show(mistage);
		}
		
		//EN CUANTO SE HAYA CONECTADO PROCEDE A ENVIAR LOS MENSAJES DE SINCRONIZACIÓN
		FileHandle fh = Gdx.files.external("DatosGames.txt");
		if(fh.exists()){
			BufferedReader reader = new BufferedReader(fh.reader());
			String line = new String();
			try {
				while((line=reader.readLine()) != null ){
					Gdx.app.log("NETWORK", "enviando: "+line);
					PacketMessage pcktmsg = new PacketMessage();
					pcktmsg.message = line;
					client.sendTCP(pcktmsg);
					//coloco una espera para que de tiempo a la BD de guardar los datos en el servidor
					TimeUnit.MILLISECONDS.sleep(500);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//POR ULTIMO SE CIERRA LA CONEXION AL SERVIDOR Y SE BORRA EL ARCHIVO
			client.close();
			if(fh.delete()){
				Gdx.app.log("FILE_HANDLE", "el archivo con los datos se elimino exitosamente");
			}else{
				Gdx.app.log("FILE_HANDLE", "No se pudo eliminar el archivo, revisar sistema de archivos");
			}
		}else{
			new Dialog("Error de Conectividad", skin, "dialog") {
				protected void result (Object object) {
					System.out.println("Chosen: " + object); 
				}
			}.text("No existen datos para sincronizar").button("Ok", true).key(Keys.ENTER, true)
			.key(Keys.ESCAPE, false).show(mistage);
		}
	}
	
	public static class PacketMessage{
		String message;
	}
	
	public MainMenuScreen(MyGdxGame unjuego)
	{
		this.mijuego = unjuego;
		//FIXME DESCOMENTAR
		/*if(mijuego.FuncionesCompletas){
			Gdx.app.log("MAIN_MENU", "si entra con FUNCIONES COMPLETAS");
		}*/
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		//******************************************************
		//INICIALIZACION DEL CLIENTE EN LA RED
		//******************************************************
		client = new Client();
		client.start();
		//registro de la clase mensaje para pasar info
	    client.getKryo().register(PacketMessage.class);
		
		mistage = new Stage();
		skin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
		Gdx.input.setInputProcessor(mistage);
		
		Gdx.app.log("MAIN_MENU", "EN LA FUNCION SHOW ANTES DE CARGAR EL BACKGROUND");
		
		prefs = Gdx.app.getPreferences("MyPrefs");
		verGraficaGdx = false;
		
		Image menubg = new Image(new Texture(Gdx.files.internal("data/imgs/menu_bg.jpg")));
		menubg.setSize(1280, 720);
		
		miskin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
		
		//CARGA DE ETXTURAS PARA LOS IMAGEBUTON
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/menus/AllMenuIcons.pack"));
		TextureRegion IconJuegos = atlas.findRegion("btnImage_menuIrAJuegos");
		Image imgIconJuegos = new Image(IconJuegos);
		TextureRegion IconConfig = atlas.findRegion("btnImage_menuIrConfig");
		Image imgIconConfig = new Image(IconConfig);
		TextureRegion IconPacientes = atlas.findRegion("btnImage_menuIrGestionPacientes");
		Image imgIconPacientes = new Image(IconPacientes);
		TextureRegion IconWireless = atlas.findRegion("btnImage_menuIrConfigInalambricas");
		Image imgIconWireless = new Image(IconWireless);
		TextureRegion IconGraficas = atlas.findRegion("btnImage_menuIrEstadisticas");
		Image imgIconGraficas = new Image(IconGraficas);
		TextureRegion IconSalir = atlas.findRegion("btnImage_menuSalir");
		Image imgIconSalir = new Image(IconSalir);
		
		//DEFINIENDO LOS IMAGEBUTONS
		Button imgButtonJugar = new Button(imgIconJuegos, miskin);
		Button imgButtonConfig = new Button(imgIconConfig, miskin);
		Button imgButtonPacientes = new Button(imgIconPacientes, miskin);
		Button imgButtonWireless = new Button(imgIconWireless, miskin);
		Button imgButtonGraficas = new Button(imgIconGraficas, miskin);
		Button imgButtonSalir = new Button(imgIconSalir, miskin);
		
		Table mitabla = new Table(miskin);
		
		startGameButton = new TextButton("Nuevo Juego", miskin);
		optionsButton = new TextButton("Opciones", miskin, "toggle");
		exitButton = new TextButton("Salir", miskin);
		
		imgButtonJugar.addListener(new ChangeListener() {
			
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				//mijuego.setScreen(new PuzzleGameScreen(mijuego));
				mijuego.setScreen(new GamesMenuScreen(mijuego));
			}
		});
		
		imgButtonPacientes.addListener(new ChangeListener() {
			
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				//PRIMERO COLOCO COMO SI ESTUVIERA DESLOGUEADO Y PASO A LA OTRA PANTALLA
				prefs.putBoolean("logueado_medico", false);
				prefs.flush();
				mijuego.setScreen(new LoginScreen(mijuego));
			}
		});
		
		imgButtonSalir.addListener(new ChangeListener() {
			
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				// TODO Auto-generated method stub
				prefs.putBoolean("logueado_medico", false);
				prefs.flush();
				Gdx.app.exit();
			}
		});
		
		imgButtonConfig.addListener(new ChangeListener() {
			
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				mijuego.setScreen(new ConfigJuegosScreen(mijuego));
			}
		});
		
		imgButtonGraficas.addListener(new ChangeListener() {
			
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				if(prefs.getString("server_address").contains(":")){
					//SI LA DIRECCION CONTIENE ´:´ entonces esta conectada via bluetooth
					mijuego.actionResolver.AbrirActividad_VerGrafica();
				}else{
					verGraficaGdx = true;
				}
			}
		});
		
		imgButtonWireless.addListener(new ChangeListener() {	
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				if(prefs.getString("server_address").contains(":")){
					//si contiene doble punto es MAC de bluetooth
					mijuego.actionResolver.AbrirActividad_SyncBT();
				}else{
					//que lo mande por wifi
					SyncPorWifi();
				}
			}
		});
		
		client.addListener(new Listener() {
			public void received (Connection connection, Object object) {
				if (object instanceof PacketMessage) {
					PacketMessage response = (PacketMessage)object;
					System.out.println(response.message);
					Gdx.app.log("NETWORK_IN", "recibio: " + response.message);			
					if(response.message.contains("|")){
						Gdx.app.log("NETWORK_IN", "ES UN MENSAJE CON RESPUESTA DESDE EL SERVIDOR");
						String[] msgPicado = response.message.split("\\|");
						//ACA ESTA COMO EJEMPLO CON LOGIN, LUEGO CAMBIAR A ALGO QUE NECESITE
						if((msgPicado[1].equalsIgnoreCase("ok")) && (msgPicado[0].equalsIgnoreCase("loginr"))){
							mijuego.FuncionesCompletas = true;
						}else if((msgPicado[1].equalsIgnoreCase("fail")) && (msgPicado[0].equalsIgnoreCase("loginr"))){
							
						}
						//FIN DE LOS IFS PARA DETERMINAR CUAL MENSAJE RECIBIO
					}
				}
			}
		});
		
		/*mitabla.setFillParent(true);
		mitabla.add(startGameButton).width(150).height(50);
		mitabla.row();
		mitabla.add(optionsButton).width(150).height(50).padTop(10);
		mitabla.row();
		mitabla.add(exitButton).width(150).height(50).padTop(10);*/
		
		mitabla.setFillParent(true);
		mitabla.add(imgButtonJugar);
		mitabla.add(imgButtonConfig);
		mitabla.add(imgButtonPacientes);
		mitabla.row();
		mitabla.add(imgButtonWireless);
		mitabla.add(imgButtonGraficas);
		mitabla.add(imgButtonSalir);
		
		mistage.addActor(menubg);
		mistage.addActor(mitabla);
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		if(verGraficaGdx){
			mijuego.setScreen(new VerGraficaScreen(mijuego));
		}

		mistage.act();
		mistage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		mistage.dispose();
		miskin.dispose();
		
	}

}
