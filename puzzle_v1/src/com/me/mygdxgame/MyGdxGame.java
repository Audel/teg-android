package com.me.mygdxgame;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class MyGdxGame extends Game {
	
	String auxLvl;
	Boolean FuncionesCompletas;
	//Valores de configuracion de los juegos
	float juegoValorUmbralPinza;
	float juegoTiempoJuegoFab;
	float juegoVelocidadPiezas;
	float juegoValorPorcentajeRelleno;
	float juegoValorTIempoJuegoPintar;
	//valores de conectividad en redes
	String networkType;
	String networkAddress;
	//dato del paciente seleccionado
	String idPacienteActual;
	
	//actionresolver para accesar la interfaz y al API de Android
	ActionResolver actionResolver;

	public MyGdxGame(ActionResolver actionResolver) {
		//INICIALIZACHUN
		this.actionResolver = actionResolver;

	}
	
	@Override
	public void create() {
		// TODO Auto-generated method stub
		//Assets.load();
		//FileHandle fhValorese = Gdx.files.local("data/config/valores.txt");
		//fhValorese.writeString("3 10 3 65 15", false);
		if(Gdx.files.local("data/config/valores.txt").exists()){
			FileHandle fhValores = Gdx.files.local("data/config/valores.txt");
			String valoresenFile = fhValores.readString();
			Gdx.app.log("MENU", valoresenFile);
			String [] listaValores = valoresenFile.split(" ");
			juegoValorUmbralPinza = Float.valueOf(listaValores[0]);
			juegoTiempoJuegoFab = Float.valueOf(listaValores[1]);
			juegoVelocidadPiezas = Float.valueOf(listaValores[2]);
			juegoValorPorcentajeRelleno = Float.valueOf(listaValores[3]);
			juegoValorTIempoJuegoPintar = Float.valueOf(listaValores[4]);
		}else{
			//creara el archivo mas adelante y lee unos valores cualequiera de el
			//Gdx.app.log("MENU", "________PUSO LOS VALORES PORQUE NO HAY ARCHIVO CREADO ");
			juegoValorUmbralPinza = 3;
			juegoTiempoJuegoFab = 10;
			juegoVelocidadPiezas = 3;
			juegoValorPorcentajeRelleno = 70;
			juegoValorTIempoJuegoPintar = 15;
		}
		
		//********DE MOMENTO ME SALTO LA SPLASHSCREEN*********
		setScreen(new BienvenidoScreen(this));
		//setScreen(new MainMenuScreen(this));
		//setScreen(new FactoryGameScreen(this));
		//setScreen(new PintarGameScreen(this));
		//setScreen(new VictoryScreen(this));
		//setScreen(new UITest(this));
		//setScreen(new GamesMenuScreen(this));
	}
	
}