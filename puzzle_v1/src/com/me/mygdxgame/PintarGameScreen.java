package com.me.mygdxgame;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import sun.management.snmp.util.MibLogger;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.me.mygdxgame.AreaPaint;
import com.me.mygdxgame.FactoryGameScreen.PiezaFabrica;

public class PintarGameScreen implements Screen{
	
	public static final int FIG_RECTANGULO = 1;
	public static final int FIG_CIRCULO = 2;
	public static final int FIG_TRIANGULO = 3;
	
	MyGdxGame mijuego;

	Stage mistage;
	ShapeRenderer mishaperenderer;
	ShapeRenderer mishaperenderer_dos;
	Boolean isTouchDown;
	
	Rectangle canvas, colorSelBox, lineSelBox, penEraBox;
	Array<DebugPieza> colorSelector;
	Rectangle densidadLine;
	Rectangle Borrador;
	FiguraGenerable figuraRandom;

	Array<Vector2> puntos_visitados;
	Vector2 mipuntoant;
	Vector3 colorActual;
	int colorActualint;
	float grosorGlobalLineas;
	
	//DE MOMENTO PARA GUARDAR DATOS
	float porRell;
	float elapsedTime;
	long actionBeginTime;
	int contAciertos;
	float promPorcentaje;

	long lastPartTime;
	int cont, piezaenuso;
	
	Pixmap pixmap;
	Texture pixmaptex;
	AreaPaint miAreaDibujo;

	String arch_level;
	Boolean takeSS;

	SpriteBatch spriteBatch;
	SpriteBatch batchPiezas;
	//Texture spriteTexture;
	Sprite sprite;
	float scrollTimer = 0.0f;
	
	//Las preferencias para comunicarse con las actividades del API ANDROID
	Preferences prefs;
	//***************************ESTADISTICAS DE JUEGO*****************************************
	FileHandle file_est;
	String chorro_estadisticas;
	long startTime;
	long distPromedioDedos;
	long tiempoCompletacion;
	int numMovimientos;
	int cierresCompletoPinza;
	private FPSLogger fpsLogger;
	
	PintarGameScreen(MyGdxGame unjuego){

		this.mijuego = unjuego;
		//*******************CAMBIAR DESPUES POR ALGO CON MAS SENTIDO********************
		//this.arch_level = new String("puzzlevistela.txt");
	}
	
	public class FiguraGenerable extends DebugPieza{

		public int tipo_figura;
		public int radio_circ;
		float perimRestarFig;
		
		public FiguraGenerable(Vector2 pos) {
			super(pos);
			this.radio_circ = (int) this.getBounds().width / 2;
		}
		
		public void UpdatePerim(float grosor){
			this.perimRestarFig = calcPerFigura(grosor);
		}
		
		public Vector2 PuntoMedio(){
			return(new Vector2( ((this.getPosition().x + this.getBounds().width)+(this.getPosition().x)) /2, 
					((this.getPosition().y + this.getBounds().height) +this.getPosition().y)/2 ) );
		}
		
		public Boolean PixelEnFigura(Vector2 pix){
			//Gdx.app.log("MyTag", "a consultar si esta el pixel en figura");
			if(tipo_figura == FIG_RECTANGULO){
				return(this.bounds.contains(pix.x, pix.y));
			}else if(tipo_figura == FIG_CIRCULO){
				return(this.radio_circ >= pix.dst(this.PuntoMedio()));
			}else if(tipo_figura == FIG_TRIANGULO){
				return( PointInTriangle(pix, this.getPosition(), 
						new Vector2(this.getPosition().x, this.position.y + this.getBounds().height), 
						new Vector2(this.getPosition().x  + this.PuntoMedio().x, this.position.y)) );
			}else{
				return false;
			}
		}
		
		public float AreaFigura(){
			if(tipo_figura == FIG_RECTANGULO){
				return(this.getBounds().height * this.getBounds().width);
			}else if(tipo_figura == FIG_CIRCULO){
				return (float) ( Math.PI * this.radio_circ * this.radio_circ);
			}else if(tipo_figura == FIG_TRIANGULO){
				return( (this.getBounds().height * this.getBounds().width)/2 );
			}else{
				return (-1);
			}
		}
		
		public float calcPerFigura(float grosorLineas){
			if(tipo_figura == FIG_RECTANGULO){
				return( (this.getBounds().height *2) + (this.getBounds().width *2) * grosorLineas);
			}else if(tipo_figura == FIG_CIRCULO){
				return (float) ( Math.PI * this.radio_circ * 2 * grosorLineas);
			}else if(tipo_figura == FIG_TRIANGULO){
				return( grosorLineas * (this.getBounds().width) * 2 *
						(new Vector2(this.position.x, this.position.y)).dst(new Vector2(this.PuntoMedio().x, this.getPosition().y + this.getBounds().height)) );
			}else{
				return (-1);
			}
		}
		
	}
	
	void agregarPuntoVisitado(){
		Vector3 punto = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0.0f);
		mistage.getCamera().unproject(punto);
		puntos_visitados.add(new Vector2(punto.x, punto.y));
	}
	
	void generarFiguraRandom(){
		this.figuraRandom = new FiguraGenerable(new Vector2(MathUtils.random(canvas.x+50, canvas.x + canvas.getWidth() -170), 
				MathUtils.random(canvas.y+50, canvas.y + canvas.getHeight() -170) ));
		this.figuraRandom.set_size(150, 150);
		this.figuraRandom.radio_circ = 150 / 2;
		int colorIndex = MathUtils.random(0, colorSelector.size -1);
		this.figuraRandom.setColor(colorSelector.get(colorIndex).getColor());
		this.figuraRandom.tipo_figura = MathUtils.random(1, 3);
	}
	
	public void saveScreenshot(FileHandle file, int x, int y, int w, int h) {
		Pixmap pixmap = getScreenshot(x, y, w, h, true);
		PixmapIO.writePNG(file, pixmap);
		pixmap.dispose();
	}
	
	public Pixmap getScreenshot(int x, int y, int w, int h, boolean flipY) {
		Gdx.gl.glPixelStorei(GL10.GL_PACK_ALIGNMENT, 1);

		final Pixmap pixmap = new Pixmap(w, h, Format.RGBA8888);
		ByteBuffer pixels = pixmap.getPixels();
		Gdx.gl.glReadPixels(x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, pixels);

		final int numBytes = w * h * 4;
		byte[] lines = new byte[numBytes];
		if (flipY) {
			final int numBytesPerLine = w * 4;
			for (int i = 0; i < h; i++) {
				pixels.position((h - i - 1) * numBytesPerLine);
				pixels.get(lines, i * numBytesPerLine, numBytesPerLine);
			}
			pixels.clear();
			pixels.put(lines);
		} else {
			pixels.clear();
			pixels.get(lines);
		}

		return pixmap;
	}
	
	public static Boolean PointInTriangle(Vector2 p, Vector2 p0, Vector2 p1, Vector2 p2)
	{
	    float s = p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * p.x + (p0.x - p2.x) * p.y;
	    float t = p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * p.x + (p1.x - p0.x) * p.y;

	    if ((s < 0) != (t < 0))
	        return false;

	    float A = -p1.y * p2.x + p0.y * (p2.x - p1.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y;
	    if (A < 0.0)
	    {
	        s = -s;
	        t = -t;
	        A = -A;
	    }
	    return s > 0 && t > 0 && (s + t) < A;
	}
	
	public float PorcentajeRelleno(Pixmap pixmap){
		
		int contPixels = 0;
		
		for(int i = 0; i<pixmap.getHeight();i++){
			for(int j = 0; j<pixmap.getWidth();j++){
				if(figuraRandom.PixelEnFigura(new Vector2(i+figuraRandom.getPosition().x, j+figuraRandom.getPosition().y)) ){
					//Gdx.app.log("MyTag", "el pixel esta en la figura");
					/*Gdx.app.log("MyTag", "los codigos de color son: " + pixmap.getPixel(i, pixmap.getHeight()-j) + " y "
							+ Color.rgba8888(new Color(colorActual.x, colorActual.y,colorActual.z, 0f)) + " e int " + colorActualint);*/
					//if(pixmap.getPixel(i, j) == Color.rgba8888(new Color(colorActual.x, colorActual.y,colorActual.z, 0f)) ){
					if(pixmap.getPixel(i, j) != -1 ){
						//Gdx.app.log("MyTag", "encontro un pixel del color ");
						contPixels++;
					}
				}
			}
		}
		Gdx.app.log("MyTag", "El numero de pixeles contados es: " + contPixels);
		return(contPixels/figuraRandom.AreaFigura());
	}
	
	public void updatepixmapL(Vector2 puntoPixMap){
		
		puntoPixMap.x -= 273;
		puntoPixMap.y = (116 + 512) - puntoPixMap.y;
				
		//Pixmap pixmap = new Pixmap( 256, 256, Format.RGBA8888 );
		
		//this.pixmapArea.setColor( 0, 1, 0, 0.75f );
		//pixmap.fillCircle( 128, 128, 128 );
		//pixmap.setColor( 1, 0, 0, 1f );
		pixmap.setColor(colorActual.x, colorActual.y, colorActual.z, 1f);
		//pixmap.drawPixel(32, 32);
		pixmap.fillCircle( (int) puntoPixMap.x, (int) puntoPixMap.y, 5);
		if(puntoPixMap.dst(mipuntoant) > 5 ){
			Gdx.gl10.glLineWidth(5);
			pixmap.drawLine( (int)mipuntoant.x, (int)mipuntoant.y, (int)puntoPixMap.x, (int)puntoPixMap.y);
		}
		//pixmaptex = new Texture( pixmap, Format.RGB888, false );
		if(pixmaptex != null) pixmaptex.dispose();
		pixmaptex = new Texture( pixmap );
		//pixmaptex.draw(pixmap, 0, 0);
		mipuntoant.x = puntoPixMap.x;
		mipuntoant.y = puntoPixMap.y;
	}

	@Override
	public void show() {
		// Cargando las preferencias
	    prefs = Gdx.app.getPreferences("MyPrefs");
		
		mistage = new Stage();
		Gdx.input.setInputProcessor(mistage);
		mishaperenderer = new ShapeRenderer();
		mishaperenderer_dos =  new ShapeRenderer();
		puntos_visitados = new Array<Vector2>();
		colorActual = new Vector3(0.0f,0.0f,0.0f);
		colorActualint = 0;
		grosorGlobalLineas = 2.0f;
		mipuntoant = new Vector2();
		spriteBatch = new SpriteBatch();
		
		pixmap = new Pixmap( 1024, 512, Format.RGBA8888 );
		//miAreaDibujo = new AreaPaint(new Pixmap( 256, 256, Format.RGBA8888 ));
		pixmaptex = new Texture( pixmap, Format.RGB888, false );
		
		takeSS = false;
		isTouchDown = false;
		
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/juego_pintar/pintar.pack"));
		TextureRegion bg_1 = atlas.findRegion("interface_dibujos");
		Image bg_pintar = new Image(bg_1);
		mistage.addActor(bg_pintar);
		
		//variables para control de mejoramiento del paciente
		contAciertos = 0;
		promPorcentaje = 0.0f;
		
		//canvas y GUI
		colorSelector = new Array<DebugPieza>();
		canvas = new Rectangle(271, 87, 970, 588);
		colorSelBox = new Rectangle(86, mistage.getHeight() - 430, 127, 343);
		lineSelBox = new Rectangle(62, mistage.getHeight()- 584, 178, 147);
		penEraBox = new Rectangle(57, mistage.getHeight()- 684, 189, 67);
		
		DebugPieza unaPTemp = new DebugPieza(new Vector2(86, mistage.getHeight() - 404) );
		unaPTemp.setColor(new Vector3(0f,0f,0f));
		unaPTemp.colorEnInt = 255;
		colorSelector.add(unaPTemp);
		//Rosa
		colorSelector.add(new DebugPieza(new Vector2(86, mistage.getHeight() - 337)));
		colorSelector.peek().setColor(new Vector3(254/255f,0f,1f) );
		colorSelector.peek().colorEnInt = -16711681;
		//Azul
		colorSelector.add(new DebugPieza(new Vector2(86, mistage.getHeight() - 275)));
		colorSelector.peek().setColor(new Vector3(0f,0f,1f) );
		colorSelector.peek().colorEnInt = 65535;
		//Amarillo
		colorSelector.add(new DebugPieza(new Vector2(86, mistage.getHeight() - 209) ));
		colorSelector.peek().setColor(new Vector3(1f,1f,1/255f) );
		colorSelector.peek().colorEnInt = -65281;
		//Rojo
		colorSelector.add(new DebugPieza(new Vector2(86, mistage.getHeight() - 143) ));
		colorSelector.peek().setColor(new Vector3(1f,0f,0f) );
		colorSelector.peek().colorEnInt = -16776961;
		//Marron
		colorSelector.add(new DebugPieza(new Vector2(153, mistage.getHeight() - 430) ));
		colorSelector.peek().setColor(new Vector3(185/255f,122/255f,87/255f) );
		colorSelector.peek().colorEnInt = Color.rgb888(185/255f,122/255f,87/255f);
		//gris
		colorSelector.add(new DebugPieza(new Vector2(153, mistage.getHeight() - 361) ));
		colorSelector.peek().setColor(new Vector3(102/255f,102/255f,102/255f) );
		colorSelector.peek().colorEnInt = 1667589119;
		//Morado
		colorSelector.add(new DebugPieza(new Vector2(153, mistage.getHeight() - 298) ));
		colorSelector.peek().setColor(new Vector3(101/255f,0f,154/255f) );
		colorSelector.peek().colorEnInt = Color.rgb888(101/255f,0f,154/255f);
		//Verde
		colorSelector.add(new DebugPieza(new Vector2(153, mistage.getHeight() - 232) ));
		colorSelector.peek().setColor(new Vector3(1/255f,1f,0f) );
		colorSelector.peek().colorEnInt = 16711935;
		//Naranja
		colorSelector.add(new DebugPieza(new Vector2(153, mistage.getHeight() - 167) ));
		colorSelector.peek().setColor(new Vector3(1f,101/255f,1/255f) );
		colorSelector.peek().colorEnInt = -10157825;
		
		//LA INICIALIZACIO DE LA PRIMERA PIEZA DEBE AHCERSE DESPUES DE LLENAR EL ARREGLO CON LOS COLORES
		generarFiguraRandom();
		//figuraRandom.calcPerFigura(grosorGlobalLineas);
		
		//iniclaliza el fpslogger
		fpsLogger = new FPSLogger();
		
		//COMIENZA EL CONTADOR
		actionBeginTime = System.nanoTime();
		//***********************************INICIA VARIABLES PARA ESTADISTICAS*******************************************
		file_est = Gdx.files.external("DatosGames.txt");
		chorro_estadisticas = new String();
		startTime = TimeUtils.nanoTime();
		distPromedioDedos = -1;
		tiempoCompletacion = (long) mijuego.juegoTiempoJuegoFab;
		numMovimientos = 0;
		cierresCompletoPinza = 0;
	}
			
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		//Gdx.gl10.glPointSize(4.3f);
		//Gdx.gl10.glLineWidth(5);
		Gdx.gl10.glPointSize(grosorGlobalLineas * 0.80f);
		Gdx.gl10.glLineWidth(grosorGlobalLineas);

		mistage.act();
		mistage.draw();
		
		if(Gdx.input.isTouched()){
			
			isTouchDown = true;
			Vector3 puntoIn = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0.0f);
			mistage.getCamera().unproject(puntoIn);
			if(canvas.contains(Gdx.input.getX(), Gdx.input.getY()) )
			{
				agregarPuntoVisitado();				
			}else if (colorSelBox.contains(puntoIn.x, puntoIn.y) ){
				for(DebugPieza unadp : colorSelector){
					if(unadp.getBounds().contains(puntoIn.x, puntoIn.y)){
						colorActual.set(unadp.getColor());
						colorActualint = unadp.colorEnInt;
						Gdx.app.log("MyTag", "el color es:" + colorActual.x + " " + colorActual.y + " " + colorActual.z + " ");
					}
				}
			}else if (lineSelBox.contains(puntoIn.x, puntoIn.y) ){
				if(puntoIn.y > lineSelBox.y+97){
					//punto mas alto
					grosorGlobalLineas = 2.0f;					
				}else if((puntoIn.y < lineSelBox.y+97) && ((puntoIn.y > lineSelBox.y+49)) ){
					grosorGlobalLineas = 8.0f;
				}else{
					grosorGlobalLineas = 16.0f;
				}
				//POR AHORA COLOCO ACA PARA PROBAR LA GENERACION DE FIGURAS DE RANDOM
				//generarFiguraRandom();
			}else if(penEraBox.contains(puntoIn.x, puntoIn.y) ){
				//takeSS = true;
			}
			
			//ESTA PARTE VA EN OTRO CONDICIONAL QUE COMENTE PARA PROBAR
			Vector3 punto = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0.0f);
			mistage.getCamera().unproject(punto);
			//miAreaDibujo.updatepixmap(new Vector2(punto.x,punto.y));
			//updatepixmapL(new Vector2(punto.x,punto.y));
		}
		
		Vector3 punto = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0.0f);
		mistage.getCamera().unproject(punto);
		
		mishaperenderer.begin(ShapeType.Point);
			mishaperenderer.setColor(colorActual.x,colorActual.y,colorActual.z,0.0f);
			//Vector2 mipuntoant = new Vector2( punto.x, punto.y);
			for(Vector2 mipunto : puntos_visitados){
				mishaperenderer.point(mipunto.x, mipunto.y, 0f);
				/*if(mipuntoant.dst(mipunto) > 7)
					mishaperenderer.line(mipunto.x, mipunto.y, mipuntoant.x, mipuntoant.y);
				mipuntoant = mipunto;*/
			}
			//Para probar el despliegue del punto medio de las figuras
			//mishaperenderer.point(figuraRandom.PuntoMedio().x, figuraRandom.PuntoMedio().y, 0f);
		mishaperenderer.end();
		
		if(puntos_visitados.size == 1) mipuntoant.set(puntos_visitados.get(0));
        if(puntos_visitados.size > 2){
            mishaperenderer.begin(ShapeType.Line);
            //mishaperenderer.setColor(0f,0f,1f,0.0f);
            mishaperenderer.setColor(colorActual.x,colorActual.y,colorActual.z,0.0f);
            for(Vector2 mipunto : puntos_visitados){
                if( (mipuntoant.dst(mipunto) > 5)
                        && (!mipuntoant.equals(puntos_visitados.get(puntos_visitados.size-1)))
                        && (!mipunto.equals(puntos_visitados.get(0))) )
                    mishaperenderer.line(mipunto.x, mipunto.y, mipuntoant.x, mipuntoant.y);
                mipuntoant = mipunto;
            }
            mishaperenderer.end();
        }
			
		if(  !Gdx.input.isTouched() && isTouchDown  ){
			Gdx.app.log("MyTag", "hizo touch up ???????????????????????????????");
			isTouchDown = false;
			//las acciones al soltar el dedo comienzan por activar el flag para verificar porcentaje de la figura
			takeSS = true;
		}
        
		//PONIENDO EL GROSOSR DE LAS LINEAS EN TAMANO DECENTE
		Gdx.gl10.glPointSize(4.3f);
		Gdx.gl10.glLineWidth(5);
		mishaperenderer_dos.begin(ShapeType.Rectangle);
			mishaperenderer_dos.setColor(Color.CYAN);
			for(DebugPieza unpieza : colorSelector){
				mishaperenderer_dos.rect(unpieza.getPosition().x, unpieza.getPosition().y, 60, 60);
			}
		mishaperenderer_dos.end();	
			
		//LOS CASOS DE CADA TIPO DE FIGURA
		if(figuraRandom.tipo_figura == FIG_RECTANGULO){
			mishaperenderer_dos.begin(ShapeType.Rectangle);
				mishaperenderer_dos.setColor(figuraRandom.getColor().x,figuraRandom.getColor().y,figuraRandom.getColor().z, 0f);
				mishaperenderer_dos.rect(figuraRandom.getPosition().x, figuraRandom.getPosition().y, figuraRandom.getBounds().width, figuraRandom.getBounds().height);
		}else if(figuraRandom.tipo_figura == FIG_CIRCULO){
			mishaperenderer_dos.begin(ShapeType.Circle);
				mishaperenderer_dos.setColor(figuraRandom.getColor().x,figuraRandom.getColor().y,figuraRandom.getColor().z, 0f);
				mishaperenderer_dos.circle(figuraRandom.PuntoMedio().x, figuraRandom.PuntoMedio().y, figuraRandom.radio_circ);
		}else if(figuraRandom.tipo_figura == FIG_TRIANGULO){
			mishaperenderer_dos.begin(ShapeType.Triangle);
				mishaperenderer_dos.setColor(figuraRandom.getColor().x,figuraRandom.getColor().y,figuraRandom.getColor().z, 0f);
				mishaperenderer_dos.triangle(figuraRandom.getPosition().x, figuraRandom.getPosition().y, 
						figuraRandom.getPosition().x + figuraRandom.getBounds().width, figuraRandom.getPosition().y,
						figuraRandom.PuntoMedio().x, figuraRandom.getPosition().y + figuraRandom.getBounds().height);
		}
			mishaperenderer_dos.end();
		
		//DESPLIEGUE DEL PIXMAP Y LAS TEXTURAS MANIPULADAS
		/*spriteBatch.begin();
			//spriteBatch.draw(miAreaDibujo.pixmapAreaTex, 273, 116);
			spriteBatch.draw(pixmaptex, 273, 116);
		spriteBatch.end();*/
			
		//PROBANDO MANEJO DE INPUT PROCESSOR
		
		//CAPTURA DE PANTALLA
		if(takeSS){
			//FileHandle imgCaptura = Gdx.files.external("mypartImage.png");
			//saveScreenshot(imgCaptura, (int) figuraRandom.getPosition().x, (int) figuraRandom.getPosition().y, 200, 200);
			//saveScreenshot(imgCaptura, 0,0, 1280,800);
			//WOLOLOLOLOOLOLOOOOLOOLOL
			Gdx.app.log("MyTag", "El array de puntos es de tamano: " + puntos_visitados.size);
			Gdx.app.log("MyTag", "A punto de calcular los rellenos");
			porRell = PorcentajeRelleno( getScreenshot( (int)figuraRandom.position.x , (int)figuraRandom.position.y, 
					(int)figuraRandom.getBounds().width, (int)figuraRandom.getBounds().height, false) );
			Gdx.app.log("MyTag", "El porcentaje es: " +  porRell);
			
			//LA PREGUNTA DEL MILLON
			if(porRell >= mijuego.juegoValorPorcentajeRelleno/100){
				Gdx.app.log("MyTag", "PORCENTAJE DE RELLENO SOBRE EL UMBRAL");
				generarFiguraRandom();
				//limpia el arreglo de puntos visitados para que siga dibujando despues
				puntos_visitados.clear();
				contAciertos++;
				if(promPorcentaje == 0.0f){
					promPorcentaje = porRell;
				}
				promPorcentaje = (promPorcentaje+porRell)/2;
			}else{
				//APROVECHO ESTE ESPACIO PARA QUE CREE EL PIXMAP MAS GRANDE Y LO COLOQUE EN EL STAGE
				Gdx.app.log("MyTag", "el stage tiene el siguiente numero de actores: " + mistage.getActors().size);
				//Updatemiimg( new Texture(getScreenshot( 275 , 120, 1024, 1024, false)) );
				//puntos_visitados.clear();
				if(mistage.getActors().size == 2){
					//YO QUERIA AGREGAR UNA CAPTURA ACTUAL DE LA PANTALLA COMO ACTOR PARA DIBUJAR ENCIMA PERO NU FUNCIONO
				}else if(mistage.getActors().size == 1){
					//mistage.addActor( new Image( new TextureRegion( new Texture(getScreenshot( 275 , 120, 1024, 1024, false)),0,440, 970,584 ) ) );
					//mistage.getActors().get(1).setPosition(275, 120);
					//FileHandle imgCaptura = Gdx.files.external("mypartImage.png");
					//saveScreenshot(imgCaptura, 275, 120, 1024, 1024);
					//puntos_visitados.clear();
				}
				Gdx.app.log("MyTag", "el stage DESPUES tiene el siguiente numero de actores: " + mistage.getActors().size);
			}
			takeSS= false;
		}
		
		elapsedTime=(System.nanoTime()-actionBeginTime)/1000000000.0f;
        if(elapsedTime > mijuego.juegoValorTIempoJuegoPintar*40){
        	//CONDICION DE FIN DE JUEGO
        	Gdx.app.log("MyTag", "el timepo de juego es "+ elapsedTime);
        	//actionBeginTime = System.nanoTime();
        	//acomodando datos para guardar en archivo
			chorro_estadisticas = "tiempo="+ String.valueOf(elapsedTime/60)+"^movimientos="+String.valueOf(numMovimientos)+
					"^promedios="+String.valueOf(promPorcentaje)+"^aciertos="+String.valueOf(contAciertos)+"^puntaje=0";
			String idG = "1004";
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			String msgHeader = "ADDGAMEDATA|"+prefs.getString("id_paciente_activo")+"|"+ timeStamp +"|"+idG+"|"+chorro_estadisticas;
			Gdx.app.log("JUEGO", "guardando en archivo: "+msgHeader );
			file_est.writeString(msgHeader+"\n", true);
			mijuego.setScreen(new VictoryScreen(mijuego));
        }
        
        //POR ULTIMO HACEMOS LOG DE LOS FPS 
        fpsLogger.log();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		mistage.dispose();
		pixmap.dispose();
		miAreaDibujo.dispose();
	}

}
