package com.me.mygdxgame;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class PuzzleGameScreen implements Screen {
	
	MyGdxGame mijuego;
	
	Stage mistage;
	ShapeRenderer mishaperenderer;
	ShapeRenderer mishaperenderer_dos;
	
	DebugPieza piezaprueba;
	Array<DebugPieza> partespuzzle;
	Array<DebugPieza> puzzlearmado;
	int cont, piezaenuso;
	Array cuenta_aciertos;
	Music mainMusic;
	
	String arch_level;
	//Las preferencias para comunicarse con las actividades del API ANDROID
	Preferences prefs;
	
	//***************************ESTADISTICAS DE JUEGO*****************************************
	FileHandle file_est;
	String chorro_estadisticas;
	long startTime;
	long distPromedioDedos;
	long tiempoCompletacion;
	int numMovimientos;
	int cierresCompletoPinza;
	Boolean notSyncd;
	private FPSLogger fpsLogger;
	
	
	PuzzleGameScreen(MyGdxGame unjuego){
		
		this.mijuego = unjuego;
		this.arch_level = unjuego.auxLvl;
		//*******************CAMBIAR DESPUES POR ALGO CON MAS SENTIDO********************
		//this.arch_level = new String("puzzlevistela.txt");
		//this.arch_level = unjuego.game_level;
	}
	
	public Vector3 puntomedio(Vector3 vA, Vector3 vB){
		
		Vector3 retorno = new Vector3();
		
		retorno.set((vA.x+vB.x)/2, (vA.y+vB.y)/2, (vA.z+vB.z)/2);
		
		return(retorno);
	}

	@Override
	public void show() {
		// Cargando las preferencias
	    prefs = Gdx.app.getPreferences("MyPrefs");
	    
	    fpsLogger = new FPSLogger();
		
		mistage = new Stage();
		Gdx.input.setInputProcessor(mistage);
		mishaperenderer = new ShapeRenderer();
		mishaperenderer_dos =  new ShapeRenderer();
		
		Sound dropSound;
		// load the drop sound effect and the rain background "music"
		//dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"));
	    mainMusic = Gdx.audio.newMusic(Gdx.files.internal("data/audio/PuzzleGameMusic.mp3"));    
	    // start the playback of the background music immediately
	    mainMusic.setLooping(true);
	    mainMusic.play();

		
		FileHandle mifile = Gdx.files.internal("data/" + arch_level);
		String texto = mifile.readString();
		String[] lineas_file = texto.split("\\r?\\n");
		String[] datos_pack = lineas_file[0].split(" ");
		String nombre_pack = datos_pack[0];
		int num_piezas = Integer.parseInt(datos_pack[1]);
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/" + nombre_pack));
		//
		TextureRegion bg_1 = atlas.findRegion(lineas_file[1]);
		Image bg_puzzle = new Image(bg_1);
		TextureRegion figura_incompleta = atlas.findRegion(lineas_file[2]);
		TextureRegion figura_armada = atlas.findRegion(lineas_file[3]);
		Image main_target = new Image(figura_incompleta);
		Image complete_target = new Image(figura_armada);
		//main_target.setColor(1.0f, 1.0f, 1.0f, 0.3f);
		TextureRegion temptexreg;
		//Array<Image> tempimg = new Array();
		Image tempimg;
		//COLOCO LOS ACTORES EN POSICION Y LOS CARGO AL ESCENARIO
		mistage.addActor(bg_puzzle);
		main_target.setPosition(600, 100);
		mistage.addActor(main_target);
		//la figura completa (VA DE TERCERA EN EL ESCENARIO)
		complete_target.setPosition(600, 100);
		complete_target.setVisible(false);
		mistage.addActor(complete_target);
		//**************************************************************
		partespuzzle = new Array();
		puzzlearmado = new Array();
		//**************************************************************
		for(cont=4;cont<num_piezas+4;cont++){
			datos_pack = lineas_file[cont].split(" ");
			temptexreg = atlas.findRegion(datos_pack[0]);
			tempimg = new Image(temptexreg);
			//la posicion deberia ser random (quizas) igual nunca cambia
			//tempimg.setPosition(new Vector2(50, 10).x, new Vector2(50, ((cont-3)*100) + 10).y);
			Vector2 posrand = new Vector2(MathUtils.random(0, 350), MathUtils.random(0, 500));
			tempimg.setPosition(posrand.x, posrand.y);
			mistage.addActor(tempimg);
			//mistage.getActors().get(mistage.getActors().size-1).setPosition(50+cont,((cont-3)*10) + 10);
			//*****************REVISAR*****************CRASHEA DESDE QUE LO PUSE*****************
			//piezaprueba = new DebugPieza(new Vector2(50, ((cont-3)*100) + 10));
			piezaprueba = new DebugPieza(posrand);
			piezaprueba.colorrgb = new Vector3(1.0f, 0.0f, 0.0f);
			piezaprueba.set_pos_target(new Vector2(main_target.getX()+Integer.parseInt(datos_pack[1]), main_target.getY()+Integer.parseInt(datos_pack[2])) );
			//DE MOMENTO SERA PARA AGARRARLA "POR LA PUNTICA" Y LUEGO ACOMODAR ESO BIEN
			piezaprueba.set_size(tempimg.getWidth(), tempimg.getHeight());
			partespuzzle.add(piezaprueba);
			piezaprueba = new DebugPieza(new Vector2(main_target.getX()+Integer.parseInt(datos_pack[1]), main_target.getY()+Integer.parseInt(datos_pack[2])) );
			piezaprueba.colorrgb = new Vector3(1.0f, 0.0f, 0.0f);
			puzzlearmado.add(piezaprueba);
		}
		
		
		cuenta_aciertos = new Array();
		
		cuenta_aciertos.add(false);
		cuenta_aciertos.add(false);
		cuenta_aciertos.add(false);
		cuenta_aciertos.add(false);
		cuenta_aciertos.add(false);
		cuenta_aciertos.add(false);
		
		//TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/prueba.pack"));
		//Image bg_nivel1 = new Image(new Texture(Gdx.files.internal("data/cielo_1.jpg")));
		
		//***********************************INICIA VARIABLES PARA ESTADISTICAS*******************************************
		file_est = Gdx.files.external("DatosGames.txt");
		chorro_estadisticas = new String();
		startTime = TimeUtils.nanoTime();
		distPromedioDedos = -1;
		tiempoCompletacion = -1;
		numMovimientos = 0;
		cierresCompletoPinza = 0;
		notSyncd = true;
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		mistage.act();
		mistage.draw();
		
		//shaperenderer de las piezas (debud) que se mueven
		mishaperenderer.begin(ShapeType.Rectangle);
			cont = 0;
			for(DebugPieza pieza : partespuzzle){
				mishaperenderer.setColor(pieza.colorrgb.x, pieza.colorrgb.y, pieza.colorrgb.z, 1.0f);
				mishaperenderer.rect(pieza.getPosition().x, pieza.getPosition().y, pieza.getBounds().width, pieza.getBounds().height);
				//ESTA ES LA ULTIMA CONDICION DE VICTORIA QUE HABRA
				if(mistage.getActors().get(2).getX() == 10){
					//acomodando datos para guardar en archivo
					if(notSyncd){
						notSyncd = false;
						chorro_estadisticas = "tiempo="+ String.valueOf(tiempoCompletacion/60000000000.0)+"^movimientos="+String.valueOf(numMovimientos)+
								"^promedios="+String.valueOf(distPromedioDedos)+"^aciertos=0^puntaje=0";
						String idG = new String();
						if(mijuego.auxLvl.equalsIgnoreCase("puzzlevistela.txt")){
							idG="1002";
						}else{
							idG="1001";
						}
						String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
						String msgHeader = "ADDGAMEDATA|"+prefs.getString("id_paciente_activo")+"|"+ timeStamp +"|"+idG+"|"+chorro_estadisticas;
						Gdx.app.log("JUEGO", "guardando en archivo: "+msgHeader );
						file_est.writeString(msgHeader+"\n", true);
						mijuego.setScreen(new VictoryScreen(mijuego));
					}									
				}
				if(Gdx.input.isTouched() &&  Gdx.input.isTouched(1)){
					Vector3 punto = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0.0f);
					Vector3 puntoDos = new Vector3(Gdx.input.getX(1), Gdx.input.getY(1), 0.0f);
					if(punto.dst(puntoDos) < 140){
						mistage.getCamera().unproject(punto);
						mistage.getCamera().unproject(puntoDos);
						//calcula punto medio para el siguiente paso
						punto = puntomedio(punto, puntoDos);
						if(pieza.getBounds().contains(punto.x, punto.y) && (piezaenuso < 0)){
							//COMIENZA A GUARDAR ESTADISTICAS
							//**************************VERIFICAR CUANDO SE ESTA REALIZANDO UN NUEVOMOVIMIENTO***************************************
							//numMovimientos = numMovimientos+1;
							if (distPromedioDedos == 0)
								distPromedioDedos = (long) punto.dst(puntoDos);
							distPromedioDedos = (long) ((distPromedioDedos + punto.dst(puntoDos)) / 2);
							if(punto.dst(puntoDos) == 0)
								cierresCompletoPinza++; 
							//PRIMERO ERA MARCAR LA POSICION PERO AHORA SIMPLEMENTE PONDRE ESA PIEZA DE ULTIMA
							//******************PROBAR OPTIMIZACION DE COMPARA SI YA ES LA ULTIMA***********************************
							DebugPieza temporal = partespuzzle.removeIndex(cont);
							partespuzzle.add(temporal);
							//Actor tempimg = mistage.getActors().removeIndex(cont+2);
							//OJO QUE CAMBIE A REMOVER cont+3 cuando se a�adio la figura incompleta: 
							Actor tempimg = mistage.getActors().removeIndex(cont+3);
							mistage.addActor(tempimg);
							//como la pieza tocada es ahora la ultima
							piezaenuso = partespuzzle.size-1;
							//*****************LE HACE EL MISMO TRATAMIENTO DE PONERLO AL FINAL PERO EN FALSE
							//Y LO CAMBIA CUANDO ESTE EN POSICION**************************************
							//cuenta_aciertos.set(cont, true);
							cuenta_aciertos.pop();
							cuenta_aciertos.add(false);
							//************************************************************************************
						}
						if(cont == piezaenuso){
							pieza.setPosition(new Vector2(punto.x-50,punto.y-50));
							mistage.getActors().get(mistage.getActors().size-1).setPosition(punto.x-50,punto.y-50);
							//la distancia deberia ser menos de la mitad del tma�o de la pieza
							boolean condvic = false;
							if(pieza.enPosicion()){
								pieza.setColor(new Vector3(1.0f,1.0f,1.0f));
								pieza.setPosition(pieza.pos_target_fig);
								mistage.getActors().get(mistage.getActors().size-1).setPosition(pieza.pos_target_fig.x, pieza.pos_target_fig.y);
								cuenta_aciertos.set(cuenta_aciertos.size -1, true);
								for(int i= 0; i<partespuzzle.size; i++){
								//if(cuenta_aciertos.contains(false, true)){
								//if(cuenta_aciertos.get(3).equals(true)){
									if(partespuzzle.get(i).enPosicion())
										condvic = true;
									else{
										condvic = false;
										break;
									}
								}
								if(condvic){
									MoveToAction action = new MoveToAction();
									action.setPosition(10, 200);
									action.setDuration(0.75f);
									//mistage.getActors().get(1).addAction(action);
									//TODO: hacer invisibles las piezas y mostrar la figura armada para que esta se desplaze
									int i;
									for(i=3;i<mistage.getActors().size;i++){
										mistage.getActors().get(i).setVisible(false);
									}
									mistage.getActors().get(1).setVisible(false);
									mistage.getActors().get(2).setVisible(true);
									mistage.getActors().get(2).addAction(action);
									mainMusic.stop();
									tiempoCompletacion = (TimeUtils.nanoTime() - startTime);
								}
								//solo una prueba, quizas lo cambie por un flag
								piezaenuso = -1;
							//CIERRA IF DE PIEZA EN POSICION	
							}
						}
					}
						
				}
				if(!Gdx.input.isTouched()){
					piezaenuso = -1;
				}
				cont++;
			}
		mishaperenderer.end();
		
		//mistage.draw();
		
		//shaperenderer del "rompecabezas" armado
		mishaperenderer_dos.begin(ShapeType.Rectangle);
			for(DebugPieza target : puzzlearmado){
				mishaperenderer_dos.setColor(target.getColor().x, target.getColor().y, target.getColor().z, 1.0f);
				mishaperenderer_dos.rect(target.getPosition().x, target.getPosition().y, 100, 100);
			}
		mishaperenderer_dos.end();
		
		//POR ULTIMO HACEMOS LOG DE LOS FPS 
        fpsLogger.log();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		mistage.dispose();
	}



}