package com.me.mygdxgame;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.me.mygdxgame.MainMenuScreen.PacketMessage;

public class VerGraficaScreen implements Screen{
	
	MyGdxGame mijuego;
	
	//VARIABLES Y ELEMENTOS DE LA UI
	Skin skin;
	Stage stage;
	SpriteBatch batch;
	Boolean leyendoMensaje, grafDescargada;

	//CLASES DE NETWORKING
	//Client object
	Client client;
	//Ports to listen on
	static int udpPort = 27967, tcpPort = 27967;
	
	Preferences prefs;
	String archRecv;
	
	Label selecJuegoLabel, selecDatoLabel;
	SelectBox selecJuego, selecDato;
	ScrollPane scrollPaneGrafica;
	Window window;
	
	public static class PacketMessage{
		String message;
	}
	public String DescargarFile(){
		//PROBANDO CON SOCKETS*********************************************************************************
		String serverName = prefs.getString("server_address");
		String nombreFile = new String();
		int port = tcpPort+1;
		int bytes = 0;
		try
		{
			//tuve que hacerle split porque lo guardaba con un �/� antes de la direccion ip
			System.out.println("Connecting to " + serverName.split("/")[1]
					+ " on port " + port);
			Socket client_socket = new Socket(serverName.split("/")[1], port);
			System.out.println("Just connected to "
					+ client_socket.getRemoteSocketAddress());
			//EN CASO DE QUERER ENVIAR ALGUN MENSAJE
			OutputStream outToServer = client_socket.getOutputStream();
			DataOutputStream out = new DataOutputStream(outToServer);
			//out.writeUTF("Hello from " + client.getLocalSocketAddress());
			//ESTE ES EL STREAM DE RECEPCION DE DATOS
			InputStream inFromServer = client_socket.getInputStream();
			DataInputStream in = new DataInputStream(inFromServer);
			//primero recibe el nombre del archivo y lo pico para saber el nombre del archivo
			String [] data_cabecera = in.readUTF().split("\\|");
			nombreFile = data_cabecera[1];
			System.out.println("Recibiendo " + data_cabecera[1]);
			//luego viene el stream de datos del archivo
			File file = new File("/mnt/sdcard/", data_cabecera[1]);
			OutputStream os = new FileOutputStream(file);
			byte [] buffer = new byte[1024];
			//Log.d(TAGMIO, "EL ENTERO BYTES VALE: " + Integer.toString(bytes));
			//DEBO CHECAR ESTA COMPARACION PARA EVITAR PROBLEMAS MAS ADELANTE
			while((bytes = in.read(buffer)) != -1){
				//ESCRIBE LOS DATOS DEL SOCKET EN EL ARCHIVO
				os.write(buffer, 0, bytes);
				os.flush();
			}
			//CIERRA EL ARCHIVO Y DEBERIA ESTAR LISTO
			os.flush();
			os.close();

			client_socket.close();
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		//FIN INCLUSION DE SOCKETS*****************************************************************************
		return nombreFile;
	}
	
	public VerGraficaScreen(MyGdxGame unjuego){
		this.mijuego = unjuego;
	}

	@Override
	public void show() {
		//******************************************************
		//INICIALIZACION DEL CLIENTE EN LA RED
		//******************************************************
		client = new Client();
		client.start();
		//registro de la clase mensaje para pasar info
		client.getKryo().register(PacketMessage.class);
		client.getKryo().register(byte[].class);
		
		// carga componentes basicos de la pantalla
		skin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		Gdx.input.setInputProcessor(stage);
		
		prefs = Gdx.app.getPreferences("MyPrefs");
		archRecv = new String();
		grafDescargada = false;
		
		
		//inicializamos los elementos de la gui y creando los estaticos
		Button botonRegresar = new TextButton("Regresar", skin);
		Button botonGrafHistorico = new TextButton("Historico", skin);
		Button botonGrafUltSesion = new TextButton("Ultima Sesion", skin);
		selecJuego = new SelectBox(new String[] {"Puzzle", "Vistela", "Fabrica", "Pintar"}, skin);
		selecDato = new SelectBox(new String[] {"Tiempo de Juego","Separaci�n de Dedos"}, skin);
		
		//A�ADIENDO EL COMPORTAMIENTO A LOS BOTOBES
		/*botonRegresar.addListener(new ChangeListener() {
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				mijuego.setScreen(new MainMenuScreen(mijuego));
			}
		});*/
		
		botonGrafHistorico.addListener(new ChangeListener() {
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				String msg = new String();
				if(selecJuego.getSelectionIndex() != 3){
					msg = "GETGRAF|"+ prefs.getString("id_paciente_activo")+ "|"+selecDato.getSelectionIndex()+
						"^" + selecJuego.getSelectionIndex()+"^0";
				}else{
					if(selecDato.getSelectionIndex()==0){
						msg = "GETGRAF|"+ prefs.getString("id_paciente_activo")+"|0^"+
								selecJuego.getSelectionIndex()+"^0";
					}else{
						msg = "GETGRAF|"+ prefs.getString("id_paciente_activo")+"|2^"+
								selecJuego.getSelectionIndex()+"^0";
					}
				}
				//envviar el mensaje por TCP//envviar el mensaje por TCP
				Gdx.app.log("NETWORK_IN","intentando establecer conexion...");
				try {
					InetAddress address = client.discoverHost(tcpPort, 5000);
					System.out.println(address);
					if(address != null){
						prefs.putString("server_address", address.toString());
						prefs.flush();
						client.connect(5000, address, tcpPort, udpPort);
					}else{
						Gdx.app.log("NETWORK_IN","NO SE ENCONTRO EL PUERTO DEL HOST, INTENTANDO DIRECTAMENTE");
						//EL SEGUNDOO VALOR DEL ARCHIVO ES LA DIRECCION Y POR ALLI DEBERIA CONECTARSE
						client.connect(5000, prefs.getString("server_address").split("/")[1], tcpPort, udpPort);
					}
				} catch (IOException e) {
					// EN ESTE CASO FALLO LA CONEXION POR ENDE DEBE MOSTRAR CUAL DE LOS METODOS USAR
					e.printStackTrace();
					Gdx.app.log("ERROR_NETWORK", e.toString());
					//E SERVIDOR NO ESTA DISPONIBLE Y DEBE MOSTRAR UN MENSAJE DE ERROR
					new Dialog("Error de Conectividad", skin, "dialog") {
						protected void result (Object object) {
							System.out.println("Chosen: " + object); 
						}
					}.text("No es posible conectarse por este medio").button("Ok", true).key(Keys.ENTER, true)
					.key(Keys.ESCAPE, false).show(stage);
				}
				
				Gdx.app.log("NETWORK", "enviando: GETGRAF|666|0^0^0");
				PacketMessage sendMsg = new PacketMessage();
				sendMsg.message = msg;
				client.sendTCP(sendMsg);
				//y cierra el cliente hecho con kryonet
				//client.close();
				//*************************************************************************
				//LOS MOVI AL LISTENER DE LA COMUNICACION CON EL SERVIDOR A VER SI ASI FUNCIONA MEJOR
				//archRecv = DescargarFile();
				//Gdx.app.log("NETWORK_IN", "EL ARCHIVO SE DEBIO HABER DESCARGADO");
            	//grafDescargada = true;
			}
		});
		
		botonGrafUltSesion.addListener(new ChangeListener() {
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				String msg = new String();
				if(selecJuego.getSelectionIndex() != 3){
					msg = "GETGRAF|"+ prefs.getString("id_paciente_activo")+ "|" + selecDato.getSelectionIndex()+ "^"+
							selecJuego.getSelectionIndex()+"^1";
				}else{
					if(selecDato.getSelectionIndex()==0){
						msg = "GETGRAF|"+ prefs.getString("id_paciente_activo")+"|0^"+
								selecJuego.getSelectionIndex()+"^1";
					}else{
						msg = "GETGRAF|"+ prefs.getString("id_paciente_activo")+"|2^"+
								selecJuego.getSelectionIndex()+"^1";
					}
				}
				
				//envviar el mensaje por TCP
				Gdx.app.log("NETWORK_IN","intentando establecer conexion...");
				try {
					InetAddress address = client.discoverHost(tcpPort, 5000);
					System.out.println(address);
					if(address != null){
						prefs.putString("server_address", address.toString());
						prefs.flush();
						client.connect(5000, address, tcpPort, udpPort);
					}else{
						Gdx.app.log("NETWORK_IN","NO SE ENCONTRO EL PUERTO DEL HOST, INTENTANDO DIRECTAMENTE");
						//EL SEGUNDOO VALOR DEL ARCHIVO ES LA DIRECCION Y POR ALLI DEBERIA CONECTARSE
						client.connect(5000, prefs.getString("server_address").split("/")[1], tcpPort, udpPort);
					}
				} catch (IOException e) {
					// EN ESTE CASO FALLO LA CONEXION POR ENDE DEBE MOSTRAR CUAL DE LOS METODOS USAR
					e.printStackTrace();
					Gdx.app.log("ERROR_NETWORK", e.toString());
					//E SERVIDOR NO ESTA DISPONIBLE Y DEBE MOSTRAR UN MENSAJE DE ERROR
					new Dialog("Error de Conectividad", skin, "dialog") {
						protected void result (Object object) {
							System.out.println("Chosen: " + object); 
						}
					}.text("No es posible conectarse por este medio").button("Ok", true).key(Keys.ENTER, true)
					.key(Keys.ESCAPE, false).show(stage);
				}
				
				Gdx.app.log("NETWORK", "enviando: GETGRAF|666|0^0^0");
				PacketMessage sendMsg = new PacketMessage();
				sendMsg.message = msg;
				client.sendTCP(sendMsg);
				//y cierra el cliente hecho con kryonet
				//client.close();
				//ESTA FUNCION EMPLEA SOCKETS DE JAVA PARA CONECTARSE Y RECIBIR UN ARCHIVO DE IMAGEN
				//*********************************TAMBIEN COMENTADO EN ESTE BOTON****************************************
				/*archRecv = DescargarFile();
				Gdx.app.log("NETWORK_IN", "EL ARCHIVO SE DEBIO HABER DESCARGADO");
            	grafDescargada = true;*/
			}
		});
		
		botonRegresar.addListener(new ChangeListener() {	
			@Override
			public void changed (ChangeEvent event, Actor actor) {
				//usa la conexion de kryonet para enviar los datos
				//PRUEBA CONECTARSE POR WIFI
				mijuego.setScreen(new MainMenuScreen(mijuego));
							
			}
		});
		
		selecJuego.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				Gdx.app.log("UITest", "Indice seleccionado: "+ selecJuego.getSelectionIndex());
				if(selecJuego.getSelectionIndex() != 3){
					selecDato.setItems(new String[] {"Tiempo de Juego","Separaci�n de Dedos"});
				}else{
					selecDato.setItems(new String[] {"Tiempo de Juego","Porcentaje de Relleno"});
				}
				
			}
		});
		
		//EL LISTENER PARA LA CONEXION DE RED
		client.addListener(new Listener() {
			public void received (Connection connection, Object object) {
				if (object instanceof PacketMessage) {
					PacketMessage response = (PacketMessage)object;
					System.out.println(response.message);
					Gdx.app.log("NETWORK_IN", "recibio: " + response.message);			
					if(response.message.contains("|")){
						Gdx.app.log("NETWORK_IN", "ES UN MENSAJE CON RESPUESTA DESDE EL SERVIDOR");
						String[] msgPicado = response.message.split("\\|");
						//ACA ESTA COMO EJEMPLO CON LOGIN, LUEGO CAMBIAR A ALGO QUE NECESITE
						if((msgPicado[1].equalsIgnoreCase("ok")) && (msgPicado[0].equalsIgnoreCase("loginr"))){
							//mijuego.FuncionesCompletas = true;
						}else if((msgPicado[1].equalsIgnoreCase("fail")) && (msgPicado[0].equalsIgnoreCase("loginr"))){

						}
						if((msgPicado[1].equalsIgnoreCase("ok")) && (msgPicado[0].equalsIgnoreCase("reqr"))){
							archRecv = DescargarFile();
							Gdx.app.log("NETWORK_IN", "EL ARCHIVO SE DEBIO HABER DESCARGADO");
							grafDescargada = true;
							client.close();
						}else if((msgPicado[1].equalsIgnoreCase("fail")) && (msgPicado[0].equalsIgnoreCase("reqr"))){
							grafDescargada = false;
							new Dialog("Error de Conectividad", skin, "dialog") {
								protected void result (Object object) {
									System.out.println("Chosen: " + object); 
								}
							}.text("No existen datos para visualizar la grafica").button("Ok", true).key(Keys.ENTER, true)
							.key(Keys.ESCAPE, false).show(stage);
							client.close();
						}
						//FIN DE LOS IFS PARA DETERMINAR CUAL MENSAJE RECIBIO
					}
				}
			}
		});
		
		//INICIALIZO LA VENTANA Y AGREGO SUS ELEMENTOS
		window = new Window("Seleccione Datos a Graficar", skin);
		window.defaults().spaceBottom(10);
		window.row();
		window.add(selecJuego);
		window.add(selecDato);
		window.add(botonGrafHistorico);
		window.add(botonGrafUltSesion);
		window.row();
		window.add(botonRegresar);
		window.row();
		window.add(scrollPaneGrafica);
		window.pack();
		
		window.setPosition( (stage.getWidth()/2) - (window.getWidth()/2), 
				((stage.getHeight()/2) - (window.getHeight()/2)) + 200);
		
		stage.addActor(window);
		
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		if(grafDescargada){
			Texture texture2 = new Texture(Gdx.files.external(archRecv));
        	TextureRegion image2 = new TextureRegion(texture2);
        	Image imageActor = new Image(image2);
        	//verfica si hay una imagen de una vista anterior
        	if(stage.getActors().size > 1){
        		Gdx.app.log("STAGE", "hay dos actores, quiero sacar al ultimo para poner la nueva imagen");
        		//stage.getActors().removeIndex(stage.getActors().size -1);
        		stage.getActors().removeIndex(0);
        	}
        	imageActor.setPosition((stage.getWidth()/2)-(imageActor.getWidth()/2) , 0f);
        	stage.addActor(imageActor);
        	Gdx.app.log("STAGE", "los actores en el stage son: " + stage.getActors().size);
			grafDescargada = false;
		}
		
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
		skin.dispose();		
	}

}
