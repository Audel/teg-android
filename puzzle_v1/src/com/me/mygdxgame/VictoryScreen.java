package com.me.mygdxgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class VictoryScreen implements Screen{

	MyGdxGame mijuego;
	
	Stage mistage;
	Image img_felicidades;
	
	VictoryScreen(MyGdxGame unjuego){
		this.mijuego = unjuego;
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		mistage =  new Stage();
		Gdx.input.setInputProcessor(mistage);
		
		img_felicidades = new Image(new Texture(Gdx.files.internal("data/imgs/felicidades.png")));
		mistage.addActor(img_felicidades);
		
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(1.0f, 1.0f, 1.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		img_felicidades.setPosition(mistage.getWidth()/2 - img_felicidades.getImageWidth()/2,  mistage.getHeight()/2 - img_felicidades.getImageHeight()/2);
		
		if(Gdx.input.justTouched())
			mijuego.setScreen(new MainMenuScreen(mijuego));
		
		mistage.act();
		mistage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
